module Options (RootOptions (RootOptions), Command (Init, Secret, Version, Token, WhoAmI), GlobalOptions (..), options) where

import Options.Applicative (Parser, ParserInfo, command, header, help, helper, hsubparser, info, long, metavar, optional, progDesc, short, strOption, switch)
import Options.Init (InitOptions, initOptions)
import Options.Secret (SecretOptions, secretOptions)
import Options.Shared (directoryOption, jsonOutputOption, outputLocationOption)

data GlobalOptions = GlobalOptions
  { verbose :: Bool,
    logFile :: Maybe FilePath,
    outputLocation :: FilePath
  }
  deriving (Show)

globalArgs :: Parser GlobalOptions
globalArgs =
  GlobalOptions
    <$> switch
      ( long "verbose"
          <> short 'v'
          -- <> metavar "VERBOSE_OUTPUT"
          <> help "Enable logging output"
      )
    <*> (optional . strOption)
      ( long "log"
          <> short 'l'
          <> metavar "FILE"
          <> help "Path to a file that will be appended to as a log"
      )
    <*> outputLocationOption False

data Command = Init InitOptions | Secret SecretOptions | Version | Token FilePath | WhoAmI FilePath Bool
  deriving (Show)

subcommand :: Parser Command
subcommand =
  hsubparser
    ( command "init" (info (Init <$> initOptions) (progDesc "Initialize this machine to communicate with Secret Server"))
        <> command "secret" (info (Secret <$> secretOptions) (progDesc "Retrieve a particular secret"))
        <> command "version" (info (pure Version) (progDesc "Show the version number"))
        <> command "token" (info (Token <$> directoryOption False) (progDesc "Get an access token to call the API, such as for items not exposed here"))
        <> command "whoami" (info (WhoAmI <$> directoryOption False <*> jsonOutputOption) (progDesc "Determine the account on Secret Server this uses"))
    )

data RootOptions = RootOptions GlobalOptions Command
  deriving (Show)

options :: ParserInfo RootOptions
options =
  info
    ( helper
        <*> ( RootOptions
                <$> globalArgs
                <*> subcommand
            )
    )
    (progDesc description <> header headerText)
  where
    headerText = "Thycotic/Delinea Secret Server Integration and SDK Client"
    description =
      "SDK and executable for access to Secret Server programatically, \
      \without requiring Kerberos (Windows) authentication or your \
      \script/application to need to deal with API keys."
