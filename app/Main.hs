{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Control.Exception (SomeException, catch, throwIO)
import Crypto.SecretServer.Sdk (clientCredentials, getBearer, initialize, load)
import Crypto.SecretServer.Sdk.Logging (LogFn, LogLevel (Debug, Info, Warn), combineLogFn, fileLogFn, stderrLogFn)
import Crypto.SecretServer.Sdk.OAuth.Json (BearerTokenExpiration (expiration), accessToken)
import Crypto.SecretServer.Sdk.SdkAccounts.Json (SdkClientAccountModel, clientAccount_clientId, clientAccount_id, clientAccount_ipAddress, clientAccount_name, clientAccount_userDisplayName, clientAccount_userId)
import Crypto.SecretServer.Sdk.Secret (deleteSecret, getSecretDetails, getSecretField, updateSecret, updateSecretField)
import Crypto.SecretServer.Sdk.Secret.Json (Launcher (launcher_name), SecretDetailGeneralUpdateModel (secretUpdateMany_folder, secretUpdateMany_name, secretUpdateMany_secretFields), SecretDetailGeneralViewModel (secretGeneralModel_active, secretGeneralModel_autoChangePassword, secretGeneralModel_enableInheritSecretPolicy, secretGeneralModel_fields, secretGeneralModel_folder, secretGeneralModel_id, secretGeneralModel_isOutOfSync, secretGeneralModel_lastHeartBeatCheck, secretGeneralModel_lastHeartBeatStatus, secretGeneralModel_launchers, secretGeneralModel_name, secretGeneralModel_outOfSyncReason, secretGeneralModel_secretPolicy, secretGeneralModel_site, secretGeneralModel_template), SecretFieldType (File, List, Password), TemplateField (templateField_description, templateField_hasHistory, templateField_isRequired, templateField_label, templateField_listType, templateField_readOnly, templateField_readOnlyReason, templateField_slug, templateField_type, templateField_value), dirtyField, secretUpdateMany_heartbeatEnabled, secretUpdateNothing)
import Crypto.SecretServer.Sdk.SharedJson (UTCTimeWithoutZone (wrappedTime), ViewFieldValue (fieldValue_description, fieldValue_hasHistory, fieldValue_isRequired, fieldValue_label), dirty, fieldValue_name, fieldValue_value)
import Crypto.SecretServer.Sdk.Users (currentUser)
import Crypto.SecretServer.Sdk.Users.Json (CurrentUserModel (..))
import Data.Aeson (ToJSON, encode)
import qualified Data.ByteString
import qualified Data.ByteString.Lazy
import Data.Maybe (fromMaybe, isJust)
import Data.Text (init, last, null, pack, unpack)
import qualified Data.Text.IO
import Data.Time.Format.ISO8601 (iso8601Show)
import Data.Version (showVersion)
import Options (Command (Init, Secret, Token, Version, WhoAmI), GlobalOptions (logFile, outputLocation, verbose), RootOptions (RootOptions), options)
import Options.Applicative (execParser)
import Options.Init (InitOptions (clientAccountName, key, keyFile, quietInit, ruleName, url))
import Options.Secret (SecretCommand (DeleteSecret, GetSecret, SetSecretField, UpdateSecret), SecretFieldValue (SecretFieldValue), SecretOptions (SecretOptions))
import Options.Shared (OutputType (Json, Print, Quiet))
import Paths_secretserver (version)
import System.IO (Handle, IOMode (AppendMode, ReadMode, WriteMode), hPutStr, hPutStrLn, hSetBinaryMode, hSetEncoding, openFile, stderr, stdin, stdout, utf8, withBinaryFile, withFile)

main :: IO ()
main = do
  hSetEncoding stdin utf8
  hSetEncoding stdout utf8
  hSetEncoding stderr utf8
  execParser options >>= decideArgs

decideArgs :: RootOptions -> IO ()
decideArgs (RootOptions global cmd) = do
  logger' <- logger
  withOutput $ \x -> decideCmd logger' x cmd
  where
    withOutput f = case outputLocation global of
      "-" -> f stdout
      v -> withFile v WriteMode (\x -> hSetEncoding x utf8 *> f x)
    stderrlog :: LogFn
    stderrlog = if verbose global then stderrLogFn Debug else stderrLogFn Warn
    logger =
      maybe
        (return stderrlog)
        (\x -> combineLogFn stderrlog . (`fileLogFn` Info) <$> openFile x AppendMode)
        (logFile global)

decideCmd :: LogFn -> Handle -> Command -> IO ()
decideCmd logger output = decideCmd'
  where
    decideCmd' Version = hPutStrLn output ("sscli: " ++ showVersion version)
    decideCmd' (WhoAmI keyfile asJson) =
      catch (load logger keyfile >>= currentUser >>= printFn) failurePrintThrow
      where
        printFn
          | asJson = outputJson output
          | otherwise = showUser output
    decideCmd' (Token keyfile) = do
      bearer <- catch (load logger keyfile >>= getBearer) failurePrintThrow
      Data.Text.IO.hPutStrLn output $ accessToken bearer
      logger Info "main" "The generated token will expire" (Just ["expiration: " ++ iso8601Show (expiration bearer)])
    decideCmd' (Init opts) = do
      let run = initialize logger (Options.Init.url opts) (keyFile opts) (ruleName opts) (key opts) (clientAccountName opts)
      client <- catch run failurePrintThrow
      let creds = clientCredentials client
      if quietInit opts then pure () else showCreds output creds
    decideCmd' (Secret (SecretOptions keyfile secretCommand)) = decideSecret logger output keyfile secretCommand

decideSecret :: LogFn -> Handle -> String -> SecretCommand -> IO ()
decideSecret logger output keyfile = decideSecret'
  where
    clientIO = load logger keyfile
    decideSecret' (GetSecret secretId (Just field) _) =
      catch
        (clientIO >>= (\x -> getSecretField x secretId field) >>= either (Data.Text.IO.hPutStr output) (\bs -> hSetBinaryMode output True >> Data.ByteString.hPutStr output bs))
        failurePrintThrow
    decideSecret' (GetSecret secretId Nothing asJson) =
      catch
        (clientIO >>= (`getSecretDetails` secretId) >>= printFn)
        failurePrintThrow
      where
        printFn
          | asJson = outputJson output
          | otherwise = showSecret output
    decideSecret' (SetSecretField secretId field isBinary inputFile outputType) = do
      catch
        (clientIO >>= (\x -> getData >>= removeTrailingNewline >>= updateSecretField x secretId field) >>= printFn)
        failurePrintThrow
      where
        getData = case (inputFile, isBinary) of
          -- Text field
          (Just "-", Nothing) -> Left <$> Data.Text.IO.hGetContents stdin
          (Nothing, Nothing) -> Left <$> Data.Text.IO.hGetContents stdin
          (Just file, Nothing) -> Left <$> withFile file ReadMode (\x -> hSetEncoding x utf8 *> Data.Text.IO.hGetContents x)
          -- Binary field
          (Just "-", Just name) -> (\x -> Right (name, x)) <$> Data.ByteString.hGetContents stdin
          (Just file, Just name) -> (\x -> Right (name, x)) <$> withBinaryFile file ReadMode Data.ByteString.hGetContents
          (Nothing, Just file) -> (\x -> Right (file, x)) <$> withBinaryFile file ReadMode Data.ByteString.hGetContents
        removeTrailingNewline l@(Left t)
          | Data.Text.null t = pure l
          | isJust inputFile = pure l
          | Data.Text.last t == '\n' = do
              logger Warn "main" "Text input has a trailing newline, this will be removed." $ Just ["If intended, this can be suppressed with explicit use of -i|--input"]
              pure $ Left $ Data.Text.init t
          | otherwise = pure l
        removeTrailingNewline r@(Right _) = pure r
        printFn = maybe (pure ()) $
          case outputType of
            Print -> Data.Text.IO.hPutStrLn output
            Json -> outputJson output
            Quiet -> const (pure ())
    decideSecret' (UpdateSecret secretId outputType newLocation fields newname heartbeat) = do
      catch
        (clientIO >>= (\x -> updateSecret x secretId update) >>= printFn)
        failurePrintThrow
      where
        update = foldl (\accum f -> f accum) secretUpdateNothing updates
        updates =
          [ maybe id (\x y -> y {secretUpdateMany_folder = Just $ dirty x}) newLocation,
            if Prelude.null fields then id else \y -> y {secretUpdateMany_secretFields = Just $ map (\(SecretFieldValue slug value) -> dirtyField slug value) fields},
            maybe id (\x y -> y {secretUpdateMany_name = Just $ dirty x}) newname,
            maybe id (\x y -> y {secretUpdateMany_heartbeatEnabled = Just $ dirty x}) heartbeat
          ]
        printFn = case outputType of
          Print -> showSecret output
          Json -> outputJson output
          Quiet -> const (pure ())
    decideSecret' (DeleteSecret secretId quiet) =
      catch
        (clientIO >>= (`deleteSecret` secretId) >>= (if quiet then const (pure ()) else outputJson output))
        failurePrintThrow

failurePrintThrow :: SomeException -> IO b
failurePrintThrow exc = putStrLn "failure" >> throwIO (exc :: SomeException)

outputJson :: (ToJSON a) => Handle -> a -> IO ()
outputJson output bs = hSetBinaryMode output True >> Data.ByteString.Lazy.hPutStr output (encode bs)

showCreds :: Handle -> SdkClientAccountModel -> IO ()
showCreds output creds =
  mapM_
    (\(k, v) -> hPutStr output k *> hPutStr output ": " *> hPutStrLn output v)
    [ ("id", show (clientAccount_id creds)),
      ("clientId", show (clientAccount_clientId creds)),
      ("ipAddress", unpack $ clientAccount_ipAddress creds),
      ("name", unpack $ clientAccount_name creds),
      ("userDisplayName", maybe "" unpack $ clientAccount_userDisplayName creds),
      ("userId", maybe "" show $ clientAccount_userId creds)
    ]

-- TODO use the `brick` library and pretty-print these differently,
-- so the columns are more lined up

showSecret :: Handle -> SecretDetailGeneralViewModel -> IO ()
showSecret output secret = do
  hPutStr output (show $ secretGeneralModel_id secret) *> Data.Text.IO.hPutStr output ": " *> Data.Text.IO.hPutStrLn output (fieldValue_value $ secretGeneralModel_name secret)
  showValue $ secretGeneralModel_enableInheritSecretPolicy secret
  showValue $ secretGeneralModel_secretPolicy secret
  showValue $ secretGeneralModel_site secret
  showValue $ secretGeneralModel_template secret
  showValue $ secretGeneralModel_active secret
  mapM_ showFieldValue $ secretGeneralModel_fields secret
  showValue $ secretGeneralModel_folder secret
  showValue $ secretGeneralModel_isOutOfSync secret
  maybe (pure ()) showValue $ secretGeneralModel_outOfSyncReason secret
  showValue $ secretGeneralModel_lastHeartBeatStatus secret
  showValueText $ maybe "never" (pack . show . wrappedTime) <$> secretGeneralModel_lastHeartBeatCheck secret
  hPutStr output "Launchers:" *> hPutStrLn output (if Prelude.null (secretGeneralModel_launchers secret) then " None" else "")
  mapM_ (Data.Text.IO.hPutStrLn output . ("  " <>) . launcher_name) $ secretGeneralModel_launchers secret
  showValue $ secretGeneralModel_autoChangePassword secret
  where
    -- showValueMaybe :: (Show a) => ViewFieldValue (Maybe a) -> IO ()
    -- showValueMaybe = showValueText . fmap (maybe "null" (pack . show))
    showValue :: (Show a) => ViewFieldValue a -> IO ()
    showValue = showValueText . fmap (pack . show)
    showValueText fv = do
      mapM_ (Data.Text.IO.hPutStr output) $ [fieldValue_label fv, ": ", fieldValue_value fv] <> name
      hPutStrLn output ""
      Data.Text.IO.hPutStr output "  " *> Data.Text.IO.hPutStrLn output (fieldValue_description fv)
      history (fieldValue_hasHistory fv == Just True)
      required (fieldValue_isRequired fv == Just True)
      where
        name
          | fieldValue_name fv == Just (fieldValue_value fv) = mempty
          | fieldValue_name fv == Just (fieldValue_label fv) = mempty
          | otherwise = maybe mempty (\x -> [" (", x, ")"]) $ fieldValue_name fv
    showFieldValue fv = do
      Data.Text.IO.hPutStr output (templateField_label fv) *> Data.Text.IO.hPutStr output ": " *> Data.Text.IO.hPutStrLn output (templateField_slug fv)
      Data.Text.IO.hPutStr output "  " *> Data.Text.IO.hPutStrLn output (templateField_description fv)
      showVal
      Data.Text.IO.hPutStr output "  Type: " *> hPutStrLn output (alttype $ templateField_type fv)
      required $ templateField_isRequired fv
      history $ templateField_hasHistory fv
      readonly (templateField_readOnly fv) (templateField_readOnlyReason fv)
      where
        showVal
          | templateField_type fv == File =
              Data.Text.IO.hPutStr output "  File name: " *> Data.Text.IO.hPutStrLn output (fromMaybe "" $ templateField_value fv)
          | templateField_type fv == List || templateField_type fv == Password =
              Data.Text.IO.hPutStrLn output "  Value: (hidden)"
          | otherwise =
              Data.Text.IO.hPutStr output "  Value: " *> Data.Text.IO.hPutStrLn output (fromMaybe "(empty)" $ templateField_value fv)
        alttype List = "List of " ++ show (templateField_listType fv)
        alttype x = show x
    history True = Data.Text.IO.hPutStrLn output "  History saved"
    history False = pure ()
    readonly True reason = mapM_ (Data.Text.IO.hPutStr output) ("  Read-only" : maybe [] (\x -> [" (", x, ")"]) reason) *> hPutStrLn output ""
    readonly False _ = pure ()
    required True = Data.Text.IO.hPutStrLn output "  Required"
    required False = pure ()

showUser :: Handle -> CurrentUserModel -> IO ()
showUser output user = do
  hPutStr output "name: "
  Data.Text.IO.hPutStrLn output $ currentUser_displayName user
  hPutStr output "username: "
  Data.Text.IO.hPutStrLn output $ currentUser_userName user
  hPutStr output "email: "
  maybe (hPutStrLn output "") (Data.Text.IO.hPutStrLn output) $ currentUser_emailAddress user
  hPutStr output "id: "
  print $ currentUser_id user
