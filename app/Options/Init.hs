{-# LANGUAGE DataKinds #-}

module Options.Init (InitOptions (..), initOptions) where

import Crypto.SecretServer.Sdk.Types (UrlPort)
import Data.Text (Text, pack)
import Options.Applicative (Parser, eitherReader, help, long, metavar, option, optional, short, strOption, (<|>))
import Options.Shared (directoryOption, fromEnvVarReader, quietOption)
import Text.Read (readEither)

data InitOptions = InitOptions
  { keyFile :: FilePath,
    url :: UrlPort,
    clientAccountName :: Maybe Text,
    ruleName :: Text,
    key :: Maybe Text,
    quietInit :: Bool
  }
  deriving (Show)

initOptions :: Parser InitOptions
initOptions =
  InitOptions
    <$> directoryOption True
    <*> option
      (eitherReader readEither)
      ( long "url"
          <> short 'u'
          <> metavar "URL"
          <> help
            "The Secret Server URL, such as https://example.com/path.\n\
            \Secret Server can only be accessed over https."
      )
    <*> (optional . strOption)
      ( long "name"
          <> short 'n'
          <> metavar "NAME"
          <> help
            "The name to give the new SDK client account.\n\
            \The device hostname is a good choice (and the default), \
            \though the purpose or function might also be useful."
      )
    <*> strOption
      ( long "rule-name"
          <> short 'r'
          <> metavar "RULE"
          <> help "The name of the onboarding rule being used"
      )
    <*> optional
      ( strOption
          ( long "key"
              <> short 'k'
              <> metavar "ONBOARDING_KEY"
              <> help "The onboarding key"
          )
          <|> option
            (eitherReader (fromEnvVarReader (Right . pack)))
            ( long "keyvar"
                <> metavar "ONBOARDING_KEY_ENVVAR"
                <> help
                  "Environment variable to read the onboarding key from.\n\
                  \For if you would rather the key not be present on the command line, \
                  \where it might be logged."
            )
      )
    <*> quietOption
