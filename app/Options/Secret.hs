{-# LANGUAGE OverloadedStrings #-}

module Options.Secret (SecretOptions (..), SecretCommand (..), secretOptions, SecretFieldValue (..)) where

import Control.Monad (when)
import Crypto.SecretServer.Sdk.Secret (SecretIdentifier, isValidSlug)
import Data.Text (Text, pack, unpack)
import Options.Applicative (Parser, auto, command, eitherReader, flag', footer, help, hsubparser, info, long, many, metavar, option, optional, progDesc, short, strOption, (<|>))
import Options.Shared (OutputType, directoryOption, inputLocationOption, jsonOutputOption, quietJsonPrintOption, quietOption, secretIdentifier)
import Text.Read (Read (readPrec), lexP, readEither)
import qualified Text.Read

data SecretCommand
  = GetSecret SecretIdentifier (Maybe Text) Bool
  | SetSecretField
      SecretIdentifier
      -- | field name
      Text
      -- | Just file name for binary files, Nothing otherwise
      (Maybe FilePath)
      -- | Input file path
      (Maybe FilePath)
      OutputType
  | UpdateSecret
      SecretIdentifier
      OutputType
      -- | new location
      (Maybe Int)
      -- | fields to update
      [SecretFieldValue]
      -- | new name
      (Maybe Text)
      -- | heartbeat
      (Maybe Bool)
  | DeleteSecret SecretIdentifier Bool
  -- \| CreateSecret
  deriving (Read, Show, Eq, Ord)

data SecretOptions = SecretOptions
  { dataFile :: FilePath,
    -- secretId :: Int,
    secretCommand :: SecretCommand
  }
  deriving (Read, Show, Eq, Ord)

data SecretFieldValue = SecretFieldValue {slugToSet :: Text, newValue :: Text}
  deriving (Eq, Ord)

instance Show SecretFieldValue where
  show (SecretFieldValue s v) = unpack s ++ "=" ++ unpack v

instance Read SecretFieldValue where
  readPrec = do
    Text.Read.String s <- lexP
    let (slug, value) = span (/= '=') s
    when (null value) $ fail "slug and value must be separated by one '='"
    either fail (pure . (`SecretFieldValue` pack value)) $ isValidSlug $ pack slug

fieldOption :: Parser Text
fieldOption =
  strOption
    ( long "field"
        <> short 'f' -- this conflicts with setFieldOption below, but you can't ever specify them both at the same time
        <> metavar "SLUG"
        <> help "The field identifier"
    )

setFieldOption :: Parser SecretFieldValue
setFieldOption =
  option
    (eitherReader readEither)
    ( long "field"
        <> short 'f' -- this conflicts with fieldOption above, but you can't ever specify them both at the same time
        <> metavar "SLUG=VALUE"
        <> help "The field identifier and its new value"
    )

binaryOption :: Parser (Maybe FilePath)
binaryOption =
  (optional . strOption)
    ( long "binary"
        <> short 'b'
        <> help
          "When a field contains a file (binary data), the file name must be supplied. \
          \If this option is supplied in conjunction with -i|--input, the \
          \file specified by -i|--input will be used but given the \
          \name specified here when uploaded. Otherwise, stdin will be read from."
    )

destFolderIdOption :: Parser Int
destFolderIdOption =
  option
    auto
    ( long "folder"
        <> short 'l' -- l for location
        <> metavar "DIR"
        <> help "Destination folder ID"
    )

renameSecretOption :: Parser Text
renameSecretOption =
  strOption
    ( long "name"
        <> short 'n'
        <> metavar "NAME"
        <> help "New name of the secret"
    )

heartbeatOption :: Parser Bool
heartbeatOption =
  flag'
    False
    ( long "no-heartbeat"
        <> help "Turn heartbeat off"
    )
    <|> flag'
      True
      ( long "heartbeat"
          <> help "Turn heartbeat"
      )

secretOptions :: Parser SecretOptions
secretOptions =
  SecretOptions
    <$> directoryOption False
    <*> hsubparser
      ( command
          "get"
          ( info
              (GetSecret <$> secretIdentifier <*> optional fieldOption <*> jsonOutputOption)
              (progDesc "Retrieve a particular secret")
          )
          <> command
            "set"
            ( info
                (SetSecretField <$> secretIdentifier <*> fieldOption <*> binaryOption <*> optional (inputLocationOption False) <*> quietJsonPrintOption)
                ( progDesc
                    "Set a secret's field. The new value will be read from stdin (or the input path specified). Useful for binary or password fields."
                    <> footer
                      "NOTE: When -i|--input is not set, a single trailing newline (if present) will be trimmed. \
                      \When entering things manually, EOF cannot usually be sent except on a blank line.\
                      \Additionally, this prevents a bug when piping things from PowerShell/pwsh (https://github.com/PowerShell/PowerShell/issues/5974)"
                )
            )
          <> command
            "move"
            ( info
                (UpdateSecret <$> secretIdentifier <*> quietJsonPrintOption <*> optional destFolderIdOption <*> many setFieldOption <*> optional renameSecretOption <*> optional heartbeatOption)
                (progDesc "Set secret properties, except for password or binary fields.")
            )
          <> command
            "delete"
            ( info
                (DeleteSecret <$> secretIdentifier <*> quietOption)
                (progDesc "Delete a secret")
            )
            -- <> command "create" (info (pure CreateSecret) (progDesc "Create a new secret"))
      )
