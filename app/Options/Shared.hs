module Options.Shared
  ( directoryOption,
    fromKeyLookupReader,
    fromEnvVarReader,
    secretIdentifier,
    jsonOutputOption,
    quietOption,
    outputLocationOption,
    inputLocationOption,
    quietJsonPrintOption,
    OutputType (Print, Json, Quiet),
  )
where

import Crypto.SecretServer.Sdk.Secret (SecretIdentifier (SecretId, SecretPath))
import Data.String (IsString)
import Options.Applicative (Parser, auto, flag, flag', help, internal, long, metavar, option, short, strOption, switch, value, (<|>))
import System.Environment (getEnvironment)
import System.IO.Unsafe (unsafePerformIO)

directoryOption ::
  (IsString s) =>
  -- | Whether to put "will be stored" instead of "is stored"
  Bool ->
  Parser s
directoryOption isFuture =
  strOption
    ( long "datafile"
        <> short 'd'
        <> metavar "KEY_FILE"
        <> help ("Path to the file where the data file " ++ (if isFuture then "will be" else "is") ++ " stored")
    )

jsonOutputOption :: Parser Bool
jsonOutputOption =
  switch
    ( long "json"
        <> help "Output as JSON rather than pretty-printed"
    )

quietOption :: Parser Bool
quietOption =
  switch
    ( long "quiet"
        <> short 'q'
        <> help "Print no output on success"
    )

outputLocationOption ::
  -- | Hide this option but still make it available for use if you know about it?
  Bool ->
  Parser FilePath
outputLocationOption isHidden =
  strOption
    ( long "output"
        <> short 'o'
        <> metavar "FILE"
        <> value "-"
        <> help "Write to this location instead of stdout (or \"-\" for stdout explicitly)"
        <> (if isHidden then internal else mempty)
    )

inputLocationOption ::
  -- | Hide this option but still make it available for use if you know about it?
  Bool ->
  Parser FilePath
inputLocationOption isHidden =
  strOption
    ( long "input"
        <> short 'i'
        <> metavar "FILE"
        <> help "Read from this location instead of stdin (or \"-\" for stdin explicitly)"
        <> (if isHidden then internal else mempty)
    )

data OutputType = Print | Json | Quiet
  deriving (Read, Show, Eq, Ord, Enum)

quietJsonPrintOption :: Parser OutputType
quietJsonPrintOption =
  flag Print Print (long "pretty-print" <> help "Output in a human-readable fashion. This is the default output result.")
    <|> flag' Json (long "json" <> help "Output as JSON rather than pretty-printed")
    <|> flag' Quiet (long "quiet" <> short 'q' <> help "Output nothing when successful.")

-- (\x -> if x then Json else Print) <$> jsonOutputOption

-- | If supplied with the name of a key, will read the
-- associated value from the list and try to parse it.
--
-- Errors on nonexistant keys though passes through empty values
--
-- This is best used with a list of environment variables from `getEnvironment`,
-- which is exactly what `fromEnvVarReader` does.
fromKeyLookupReader :: [(String, String)] -> (String -> Either String a) -> String -> Either String a
fromKeyLookupReader kv f var = maybe (Left ("no entry for key " ++ show var)) Right (lookup var kv) >>= f

-- | Supply the variables from the environment to `fromKeyLookupReader`
--
-- So long as you don't modify the environment while parsing options, it's safe.
fromEnvVarReader :: ((String -> Either String a) -> String -> Either String a)
{-# NOINLINE fromEnvVarReader #-}
fromEnvVarReader = fromKeyLookupReader $ unsafePerformIO getEnvironment

secretIdentifier :: Parser SecretIdentifier
secretIdentifier =
  secretId <|> secretPath
  where
    secretPath =
      SecretPath
        <$> strOption
          ( long "secretpath"
              <> metavar "SECRET_PATH"
              <> help "Full path (including folder and secret name) to look up secret by"
          )
    secretId =
      SecretId
        <$> option
          auto
          ( long "secret"
              <> short 's'
              <> metavar "SECRET_ID"
              <> help "The ID of the secret"
          )
