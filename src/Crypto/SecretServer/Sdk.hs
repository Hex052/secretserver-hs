{-# LANGUAGE CPP #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Crypto.SecretServer.Sdk
  ( Client,
    clientCredentials,
    url,
    logger,
    getBearer,
    initialize,
    load,
    save,
    version,
    withClient,
    -- setPath,
  )
where

import Control.Concurrent (MVar, newMVar, putMVar, takeMVar)
import Control.Exception (Exception (displayException), SomeException, bracket, bracketOnError, catch, throwIO)
import Control.Monad (when)
import Crypto.SecretServer.Sdk.Logging (LogFn, LogLevel (Error, Trace))
import Crypto.SecretServer.Sdk.OAuth (getBearerTokenFromSdkClientAccount)
import Crypto.SecretServer.Sdk.OAuth.Json (BearerTokenExpiration, isBearerExpired)
import Crypto.SecretServer.Sdk.SdkAccounts (create)
import Crypto.SecretServer.Sdk.SdkAccounts.Json (SdkClientAccountModel)
import Crypto.SecretServer.Sdk.Types (UrlPort)
import Data.Aeson (FromJSON (parseJSON), KeyValue ((.=)), ToJSON (toEncoding, toJSON), encodeFile, object, pairs, throwDecodeStrict, withObject, (.:))
import Data.ByteString (readFile)
import Data.Functor (($>))
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import Data.Maybe (fromJust, fromMaybe)
import Data.Text (Text, pack)
import qualified Data.Text as T
import Data.Time (defaultTimeLocale, formatTime, getCurrentTime)
import Data.Version (showVersion)
import GHC.Generics (Generic)
import GHC.IO (unsafePerformIO)
import Network.HostName (getHostName)
import Paths_secretserver (version)
import System.Directory (doesPathExist)
import System.IO.Error (alreadyExistsErrorType, mkIOError)
import System.Info (arch, os)
import Prelude hiding (readFile)

data Client = Client
  { -- | Base URL and port of the client
    url :: UrlPort,
    -- | Filepath where the client is saved to
    storageLocation :: Maybe FilePath,
    -- | Logger (not saved)
    logger :: Maybe LogFn,
    -- | Client credentials
    clientCredentials :: SdkClientAccountModel,
    -- | Enables a token to be used across several requests
    token :: (MVar (), IORef (Maybe BearerTokenExpiration))
    -- HEY! If you add/modify/remove fields, be sure to also change the definitions of ToJSON and FromJSON
  }
  deriving (Generic)

instance ToJSON Client where
  toEncoding client = pairs ("url" .= url client <> "clientCredentials" .= clientCredentials client)
  toJSON client = object ["url" .= url client, "clientCredentials" .= clientCredentials client]

instance FromJSON Client where
  parseJSON = withObject "Client" $ \v -> Client <$> v .: "url" <*> pure Nothing <*> pure Nothing <*> v .: "clientCredentials" <*> pure (unsafePerformIO lockAndToken)

lockAndToken :: IO (MVar (), IORef (Maybe a))
lockAndToken = do
  l <- newMVar ()
  r <- newIORef Nothing
  pure (l, r)

-- | Get a bearer token, either a new one or an existing (unexpired) one
getBearer :: Client -> IO BearerTokenExpiration
getBearer client = bracketOnError (takeMVar lock) (putMVar lock) body
  where
    -- TODO add some logging here
    (lock, token') = token client
    newToken = getBearerTokenFromSdkClientAccount (fromJust $ logger client) (url client) (clientCredentials client)
    saveToken = newToken >>= (\x -> writeIORef token' (Just x) $> x)
    body _ = do
      readIORef token' >>= maybe saveToken fromMaybeExpired
    fromMaybeExpired t = do
      expired <- isBearerExpired t
      if expired then saveToken else pure t

-- | Initialize a new connection to Secret Server.
--
-- As part of this, it immidately calls `save`.
-- Will raise an `IOError` if the file already exists.
initialize ::
  LogFn ->
  -- | Base URL of the Secret Server instance
  UrlPort ->
  -- | Path to storage file
  FilePath ->
  -- | Name of the onboarding profile
  Text ->
  -- | Onboarding key
  Maybe Text ->
  -- | Name of the SDK Client account to be created
  Maybe Text ->
  IO Client
initialize logger' urlPort path name key clientName = do
  -- catchIOError action catch
  pathExists <- doesPathExist path
  when (pathExists || null path) doPathExists
  logger' Trace "Crypto.Secretserver.Sdk.initialize" "Checked if storage path exists, it does not" $ Just ["path: " ++ show path]
  hostname <- pack <$> getHostName
  descr <- description (Just hostname)
  let clientName' = fromMaybe hostname clientName
  logger' Trace "Crypto.Secretserver.Sdk.initialize" "Sending request" $ Just ["clientName: " ++ show clientName, "description: " ++ show descr, "profile: " ++ show name, "key: " ++ show (key *> Just ("****" :: String))]
  resp <- catch (create logger' urlPort Nothing descr clientName' key name) errcreate
  client <- Client urlPort (Just path) (Just logger') resp <$> lockAndToken
  save client
  pure client
  where
    doPathExists = do
      logger' Error "Crypto.Secretserver.Sdk.initialize" "Checked if storage path exists, it does" $ Just ["path: " ++ show path]
      ioError $ mkIOError alreadyExistsErrorType "Crypto.Secretserver.Sdk.initialize" Nothing (Just path)
    errcreate exc = do
      logger' Error "Crypto.Secretserver.Sdk.initialize" "Error with HTTP request" (Just [displayException exc])
      throwIO (exc :: SomeException)

description ::
  -- | Hostname
  Maybe Text ->
  IO Text
description hostname = do
  host <- maybe (pack <$> getHostName) pure hostname
  time <- getCurrentTime
  let descriptionLines =
        [ "sscli " `T.append` pack (showVersion version),
          "hostname: " `T.append` host,
          T.concat ["os: ", pack os, " ", pack arch],
          "created: " `T.append` pack (formatTime defaultTimeLocale "%Y-%m-%d %H:%M:%S" time)
        ]
  return $ T.intercalate "\n" descriptionLines

-- | Loads a client from a file.
--
-- | Might throw an `IOException` or `AesonException`
load ::
  LogFn ->
  -- | Path to storage file
  String ->
  IO Client
load logger' path =
  catch
    (readFile path >>= throwDecodeStrict >>= (\x -> pure $ x {storageLocation = Just path, logger = Just logger'}))
    reraise
  where
    reraise e = do
      logger' Error "Crypto.SecretServer.Sdk.load" "Could not load from file" $ Just ["path: " ++ show path, displayException e]
      throwIO (e :: SomeException)

-- | Saves the current configuration and any changes that were made.
-- Must be called after using the client.
save :: Client -> IO ()
save client = catch (encodeFile path client) reraise
  where
    path = fromJust $ storageLocation client
    reraise e = do
      (fromJust $ logger client) Error "Crypto.SecretServer.Sdk.save" "Could not save to file" $ Just ["path: " ++ show path, displayException e]
      throwIO (e :: SomeException)

-- | Like `withFile` or `bracket` but for a `Client` object
withClient :: LogFn -> String -> (Client -> IO c) -> IO c
withClient logger' path = bracket (load logger' path) save

-- setPath ::
--   -- | New save path
--   String ->
--   -- | Delete the old save path?
--   -- You probably do want to delete it, if you're moving it.
--   -- Copying it will result in strange errors.
--   Bool ->
--   -- | Client to change the save path of
--   Client ->
--   IO Client
-- setPath newPath deleteOld client =
--   if deleteOld then removeFile (storageLocation client) >> result else result
--   where
--     result =
--       return
--         client {storageLocation = newPath}
