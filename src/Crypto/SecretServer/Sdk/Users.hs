{-# LANGUAGE OverloadedStrings #-}

module Crypto.SecretServer.Sdk.Users (currentUser) where

import Control.Exception (catch)
import Crypto.SecretServer.Sdk (Client (logger, url), getBearer)
import Crypto.SecretServer.Sdk.Error (handleError, onOtherThrow)
import Crypto.SecretServer.Sdk.Logging (LogLevel (Trace))
import Crypto.SecretServer.Sdk.OAuth.Json (tokenHeaderValue)
import Crypto.SecretServer.Sdk.Types (reqPort, reqUrl)
import Crypto.SecretServer.Sdk.Users.Json (CurrentUserModel)
import Data.Maybe (fromJust)
import Network.HTTP.Req (GET (GET), NoReqBody (NoReqBody), defaultHttpConfig, jsonResponse, req, responseBody, runReq, (/:))

currentUser :: Client -> IO CurrentUserModel
currentUser client = do
  logger' Trace loc ("GET " ++ show url') Nothing
  bt <- getBearer client
  responseBody <$> catch (callFn bt) (handleError logger' loc onOtherThrow onOtherThrow)
  where
    logger' = fromJust $ logger client
    urlPort = url client
    url' = reqUrl urlPort /: "api" /: "v1" /: "users" /: "current"
    callFn bt =
      runReq defaultHttpConfig $
        req GET url' NoReqBody jsonResponse (reqPort urlPort <> tokenHeaderValue bt)
    loc = "Crypto.SecretServer.Sdk.OAuth.currentUser"
