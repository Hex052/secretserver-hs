{-# LANGUAGE DeriveGeneric #-}

module Crypto.SecretServer.Sdk.Users.Json (CurrentUserModel (..)) where

import Crypto.SecretServer.Sdk.Internal (optionsOmitNothing, trimFront)
import Crypto.SecretServer.Sdk.SharedJson (WrappedName)
import Data.Aeson (FromJSON, ToJSON, genericParseJSON, genericToEncoding, genericToJSON)
import Data.Aeson.Types (FromJSON (parseJSON), ToJSON (toEncoding, toJSON))
import Data.Text (Text)
import GHC.Generics (Generic)

data CurrentUserModel = CurrentUserModel
  { -- | List of admin link options for current user
    currentUser_adminLinks :: [MenuLink],
    -- | Date option of current user",
    currentUser_dateOptionId :: Int,
    -- | Display Name of current user
    currentUser_displayName :: Text,
    -- | Email Address of current user
    currentUser_emailAddress :: Maybe Text,
    -- | Id of current user
    currentUser_id :: Int,
    -- | Permissions assigned to current user,
    currentUser_permissions :: [WrappedName Text],
    currentUser_platformIntegrationType :: PlatformIntegrationType,
    -- | List of profile options for current user
    currentUser_profileLinks :: [MenuLink],
    -- | Time option of current user
    currentUser_timeOptionId :: Int,
    -- | Language of current user
    currentUser_userLcid :: Int,
    -- | User Name of current user
    currentUser_userName :: Text,
    -- | The current user's theme
    currentUser_userTheme :: Text
  }
  deriving (Generic, Read, Show, Eq, Ord)

instance ToJSON CurrentUserModel where
  toEncoding = genericToEncoding $ trimFront "currentUser_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "currentUser_" optionsOmitNothing

instance FromJSON CurrentUserModel where
  parseJSON = genericParseJSON $ trimFront "currentUser_" optionsOmitNothing

data MenuLink = MenuLink
  { menuLink_link :: Text,
    menuLink_name :: Text
  }
  deriving (Generic, Read, Show, Eq, Ord)

instance ToJSON MenuLink where
  toEncoding = genericToEncoding $ trimFront "menuLink_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "menuLink_" optionsOmitNothing

instance FromJSON MenuLink where
  parseJSON = genericParseJSON $ trimFront "menuLink_" optionsOmitNothing

-- | Type of Platform integration (gee, thanks API docs...)
data PlatformIntegrationType = None | Native | Hybrid
  deriving (Generic, Read, Show, Eq, Ord, Enum)

instance ToJSON PlatformIntegrationType where
  toEncoding = genericToEncoding optionsOmitNothing
  toJSON = genericToJSON optionsOmitNothing

instance FromJSON PlatformIntegrationType where
  parseJSON = genericParseJSON optionsOmitNothing
