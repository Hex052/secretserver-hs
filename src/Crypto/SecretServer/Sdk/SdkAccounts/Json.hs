{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

-- | This module contains all the models that the server returs and we have to send.
module Crypto.SecretServer.Sdk.SdkAccounts.Json
  ( ClientCreateArgs (..),
    clientCreateArgsRandomId,
    SdkClientAccountModel (..),
  )
where

import Crypto.SecretServer.Sdk.Internal (optionsOmitNothing, trimFront)
import Data.Aeson (FromJSON (parseJSON), ToJSON (toEncoding, toJSON), genericParseJSON, genericToEncoding, genericToJSON)
import Data.Functor ((<&>))
import Data.Text (Text)
import Data.UUID.Types (UUID)
import Data.UUID.V4 (nextRandom)
import GHC.Generics (Generic)

data ClientCreateArgs = ClientCreateArgs
  { clientCreate_clientId :: UUID,
    clientCreate_description :: Text,
    clientCreate_name :: Text,
    clientCreate_onboardingKey :: Maybe Text,
    clientCreate_ruleName :: Text
  }
  deriving (Generic, Read, Show, Eq)

instance ToJSON ClientCreateArgs where
  toEncoding = genericToEncoding $ trimFront "clientCreate_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "clientCreate_" optionsOmitNothing

instance FromJSON ClientCreateArgs where
  parseJSON = genericParseJSON $ trimFront "clientCreate_" optionsOmitNothing

clientCreateArgsRandomId :: Text -> Text -> Maybe Text -> Text -> IO ClientCreateArgs
clientCreateArgsRandomId description' name' onboardingKey' ruleName' =
  nextRandom <&> (\x -> ClientCreateArgs x description' name' onboardingKey' ruleName')

data SdkClientAccountModel = SdkClientAccountModel
  { -- | This is the passed-in `clientCreate_clientId` field of `ClientCreateArgs`
    --
    -- Before using this field the .NET SDK prepends "sdk-client-" to it and the grant type is always "client_credentials"
    clientAccount_clientId :: UUID,
    -- | The secret to generate a bearer token
    clientAccount_clientSecret :: Text,
    -- | This is the passed-in `clientCreate_description` field of `ClientCreateArgs`
    clientAccount_details :: Text,
    clientAccount_id :: Int,
    -- | The IP address able to use this token
    clientAccount_ipAddress :: Text,
    -- | This is _almost_ the name passed in with `clientCreate_name`
    -- though has the UTC date and time of creation appended
    -- in the format "%-m/%-d/%Y %-l:%M:%S %p"
    clientAccount_name :: Text,
    -- | Usually null
    clientAccount_userDisplayName :: Maybe Text,
    -- | Usually null
    clientAccount_userId :: Maybe Int
  }
  deriving (Generic, Read, Show, Eq)

instance ToJSON SdkClientAccountModel where
  toEncoding = genericToEncoding $ trimFront "clientAccount_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "clientAccount_" optionsOmitNothing

instance FromJSON SdkClientAccountModel where
  parseJSON = genericParseJSON $ trimFront "clientAccount_" optionsOmitNothing
