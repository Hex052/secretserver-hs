{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Crypto.SecretServer.Sdk.OAuth.Json
  ( TokenResponse,
    BearerTokenExpiration (..),
    toBearerTokenExpiration,
    isBearerExpired,
    tokenHeaderValue,
    -- BearerToken (BearerToken),
  )
where

import Data.Aeson (FromJSON (parseJSON), ToJSON (toJSON), Value (String), withText)
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)
import Data.Time (UTCTime, getCurrentTime)
import Data.Time.Clock (NominalDiffTime, addUTCTime)
import GHC.Generics (Generic)
import Network.HTTP.Req (Option, Scheme (Https), oAuth2Bearer)
import Text.Read (lexP, readPrec)
import qualified Text.Read

data TokenResponse = TokenResponse
  { access_token :: Text,
    token_type :: TokenType,
    -- | Expiration time in seconds
    expires_in :: Int,
    refresh_token :: Maybe Text
  }
  deriving (Generic, Read, Show, Eq)

instance ToJSON TokenResponse

instance FromJSON TokenResponse

data TokenType = Bearer
  deriving (Eq)

instance Show TokenType where
  show _ = "bearer"

instance Read TokenType where
  readPrec = do
    Text.Read.String s <- lexP
    if s == "bearer" then pure Bearer else fail "TokenType can only be \"bearer\""

instance ToJSON TokenType where
  toJSON _ = String "bearer"

instance FromJSON TokenType where
  parseJSON = withText "TokenType" $ \s -> if s == "bearer" then pure Bearer else fail "TokenType can only be \"bearer\""

data BearerTokenExpiration = BearerTokenExpiration
  { accessToken :: Text,
    refreshToken :: Maybe Text,
    expiration :: UTCTime
  }
  deriving (Generic, Read, Show, Eq)

tokenHeaderValue :: BearerTokenExpiration -> Option 'Https
tokenHeaderValue = oAuth2Bearer . encodeUtf8 . accessToken

instance ToJSON BearerTokenExpiration

instance FromJSON BearerTokenExpiration

toBearerTokenExpiration :: TokenResponse -> IO BearerTokenExpiration
toBearerTokenExpiration resp =
  BearerTokenExpiration (access_token resp) (refresh_token resp)
    . addUTCTime (fromIntegral (expires_in resp - 5) :: NominalDiffTime)
    <$> getCurrentTime

isBearerExpired :: BearerTokenExpiration -> IO Bool
isBearerExpired token = (expiration token <) <$> getCurrentTime

-- newtype BearerToken = BearerToken ByteString
--   deriving (Read, Show, Eq)

-- instance FromJSON BearerToken where
--   parseJSON = withText "BearerToken" (pure . BearerToken . encodeUtf8)

-- instance ToJSON BearerToken where
--   toJSON (BearerToken t) = String $ decodeUtf8 t
