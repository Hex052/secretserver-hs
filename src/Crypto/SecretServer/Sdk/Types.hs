{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Crypto.SecretServer.Sdk.Types (UrlPort (UrlPort), reqUrl, reqPort, toUrl) where

import Control.Exception (Exception (displayException), SomeException)
import Data.Aeson (FromJSON (parseJSON), ToJSON (toJSON), Value (String), withText)
import Data.Bifunctor (Bifunctor (first))
import Data.Text (Text, pack)
import qualified Data.Text
import Network.HTTP.Req (Option, Scheme (Https), Url, https, port, renderUrl, (/:))
import Text.Read (Read (readPrec), (<++))
import qualified Text.Read
import Text.URI (Authority (authHost, authPort, authUserInfo), URI (uriAuthority, uriFragment, uriPath, uriQuery, uriScheme), mkScheme, mkURI, unRText)

data UrlPort = UrlPort (Url 'Https) (Maybe Word)

instance ToJSON UrlPort where
  toJSON = String . displayUrlPort

instance FromJSON UrlPort where
  parseJSON = withText "UrlPort" (either fail pure . toUrl)

instance Show UrlPort where
  show = Data.Text.unpack . displayUrlPort

instance Read UrlPort where
  readPrec = next []
    where
      next accum =
        (Text.Read.get >>= \x -> next (x : accum))
          <++ either fail pure (toUrl $ Data.Text.reverse $ pack accum)

displayUrlPort :: UrlPort -> Text
displayUrlPort (UrlPort url_ Nothing) = renderUrl url_
displayUrlPort (UrlPort url_ (Just 443)) = renderUrl url_
displayUrlPort (UrlPort url_ (Just port_)) = Data.Text.concat ["https://", host, ":", Data.Text.pack (show port_), path]
  where
    urlStr = Data.Text.drop 8 $ renderUrl url_
    (host, path) = Data.Text.break (== '/') urlStr

toUrl :: Text -> Either String UrlPort
toUrl str = do
  uri <- first (("bad url: " ++) . displayException) (mkURI str :: Either SomeException URI)
  if uriScheme uri /= mkScheme "https" then Left "bad url: must be https" else Right ()
  authority <- first (const "bad url: missing fqdn") $ uriAuthority uri
  let host = https $ unRText $ authHost authority
  let ssUrl = maybe host (\(_, pathComponents) -> foldl (/:) host (unRText <$> pathComponents)) $ uriPath uri
  maybe (Right ()) (const $ Left "bad url: must not contain a username or password") $ authUserInfo authority
  if not (null (uriQuery uri)) then Left "bad url: may not have query options applied" else Right ()
  maybe (Right ()) (const $ Left "bad url: may not have fragment") $ uriFragment uri
  let portNum = authPort authority >>= \x -> if x == 443 then Nothing else Just x
  return $ UrlPort ssUrl portNum

reqUrl :: UrlPort -> Url 'Https
reqUrl (UrlPort u _) = u

reqPort :: UrlPort -> Option scheme
reqPort (UrlPort _ p) = maybe mempty (port . fromIntegral) p
