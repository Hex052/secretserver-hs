module Crypto.SecretServer.Sdk.Logging
  ( LogLevel (..),
    LogFn,
    nullLogFn,
    stderrLogFn,
    combineLogFn,
    fileLogFn,
  )
where

import Control.Monad (unless)
import Data.List (intercalate)
import Data.Time (defaultTimeLocale, formatTime, getCurrentTime)
import System.IO (Handle, hFlush, hPutStrLn, stderr)

data LogLevel = Trace | Debug | Info | Warn | Error | Critical
  deriving (Bounded, Show, Read, Enum, Ord, Eq)

type LogFn =
  LogLevel ->
  -- | The source of the log message
  String ->
  -- | The log message
  String ->
  -- | Possibly some additional data
  Maybe [String] ->
  IO ()

nullLogFn :: LogFn
nullLogFn _ _ _ _ = return ()

-- | Writes to an open stream (be sure it's writable and in text mode)
fileLogFn :: Handle -> LogLevel -> LogFn
fileLogFn handle minLevel level src msg extra =
  unless
    (level < minLevel)
    (formatLogMsg level src msg extra >>= hPutStrLn handle >> hFlush handle)

-- | Writes to stderr
stderrLogFn :: LogLevel -> LogFn
stderrLogFn = fileLogFn stderr

-- | Writes to two log functions
combineLogFn :: LogFn -> LogFn -> LogFn
combineLogFn a b level src msg extra = a level src msg extra >> b level src msg extra

formatLogMsg :: LogLevel -> String -> String -> Maybe [String] -> IO String
formatLogMsg level src msg extra = do
  time <- getCurrentTime
  let fmsg = "[" ++ formatTime defaultTimeLocale "%Y-%m-%d %H:%M:%S" time ++ "][" ++ show level ++ "][" ++ src ++ "]" ++ msg
  return $ maybe fmsg (\x -> intercalate "\n" (fmsg : map ("    " ++) x)) extra
