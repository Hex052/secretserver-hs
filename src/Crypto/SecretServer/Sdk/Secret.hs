{-# LANGUAGE OverloadedStrings #-}

module Crypto.SecretServer.Sdk.Secret
  ( isValidSlug,
    getSecret,
    getSecretDetails,
    getSecretField,
    SecretIdentifier (SecretId, SecretPath),
    deleteSecret,
    moveSecret,
    updateSecret,
    updateSecretField,
  )
where

import Control.Exception (catch, throwIO)
import Control.Monad (unless, when)
import Crypto.SecretServer.Sdk (Client, getBearer, logger, url)
import Crypto.SecretServer.Sdk.Error (ApiError (BadContentType, BadJson200), handleError, onOtherThrow)
import Crypto.SecretServer.Sdk.Internal (extendedModel)
import Crypto.SecretServer.Sdk.Logging (LogLevel (Trace))
import Crypto.SecretServer.Sdk.OAuth.Json (tokenHeaderValue)
import Crypto.SecretServer.Sdk.Secret.Json (SecretDetailGeneralUpdateModel (secretUpdateMany_folder), SecretDetailGeneralViewModel, SecretModelV2, secretUpdateNothing)
import Crypto.SecretServer.Sdk.SharedJson (Data (Data), DeletedModel, dirty)
import Crypto.SecretServer.Sdk.Types (reqPort, reqUrl)
import Data.Aeson (KeyValue ((.=)), eitherDecodeStrict, object)
import Data.ByteString (ByteString, takeWhile)
import Data.Char (isLetter)
import Data.Maybe (fromJust)
import Data.String (IsString)
import Data.Text (Text)
import qualified Data.Text as T
import Network.HTTP.Client (RequestBody (RequestBodyBS))
import Network.HTTP.Client.MultipartFormData (partFileRequestBodyM)
import Network.HTTP.Req (DELETE (DELETE), GET (GET), HttpException (JsonHttpException), NoReqBody (NoReqBody), PATCH (PATCH), PUT (PUT), QueryParam (queryParam), ReqBodyJson (ReqBodyJson), bsResponse, defaultHttpConfig, jsonResponse, req, reqBodyMultipart, responseBody, responseHeader, runReq, (/:), (/~), (=:))
import System.FilePath (takeFileName)
import Prelude hiding (takeWhile)

-- isValidSlug :: Text -> Either Text Text
isValidSlug :: (IsString a) => Text -> Either a Text
isValidSlug slug = do
  when (T.null slug) $ Left "slug may not be empty (though value can)"
  unless (T.all (\x -> isLetter x || x == '-') slug) $ Left "slug must be only letters (characters that pass `isLetter`) or hyphens ('-')."
  pure slug

data SecretIdentifier = SecretId Int | SecretPath Text
  deriving (Read, Show, Eq, Ord)

secretIdNum :: SecretIdentifier -> Int
secretIdNum (SecretId s) = s
secretIdNum (SecretPath _) = 0

secretIdParam :: (Monoid a, QueryParam a) => SecretIdentifier -> a
secretIdParam (SecretId _) = mempty
secretIdParam (SecretPath s) = queryParam "secretPath" $ Just s

getSecret :: Client -> SecretIdentifier -> IO SecretModelV2
getSecret client secretId = do
  logger' Trace loc ("GET " ++ show url') Nothing
  bt <- getBearer client
  responseBody <$> catch (callFn bt) (handleError logger' loc onOtherThrow onOtherThrow)
  where
    loc = "Crypto.SecretServer.Sdk.Secret.getSecret"
    logger' = fromJust $ logger client
    urlPort = url client
    url' = reqUrl urlPort /: "api" /: "v1" /: "secrets" /~ secretIdNum secretId
    callFn bt =
      runReq defaultHttpConfig $
        req GET url' NoReqBody jsonResponse (reqPort urlPort <> secretIdParam secretId <> tokenHeaderValue bt)

-- | Broadly the same as `getSecret`, returning more details.
--
-- There is an /undocumented/ header option @extended-model: extended@ that
-- enables returning `SecretDetailGeneralViewModel`` instead of the same with the
-- `fieldValue_value` fields in place of the `ViewFieldValue` ones.
getSecretDetails :: Client -> SecretIdentifier -> IO SecretDetailGeneralViewModel
getSecretDetails client secretId = do
  logger' Trace loc ("GET " ++ show url') Nothing
  bt <- getBearer client
  responseBody <$> catch (callFn bt) (handleError logger' loc onOtherThrow onOtherThrow)
  where
    loc = "Crypto.SecretServer.Sdk.Secret.getSecretDetails"
    logger' = fromJust $ logger client
    urlPort = url client
    url' = reqUrl urlPort /: "api" /: "v1" /: "secrets" /: "secret-detail" /~ secretIdNum secretId /: "general"
    callFn bt =
      runReq defaultHttpConfig $
        req GET url' NoReqBody jsonResponse (reqPort urlPort <> queryParams <> headers)
      where
        headers = extendedModel <> tokenHeaderValue bt
        queryParams = "isEditMode" =: False <> "loadReadOnlyFlags" =: True <> secretIdParam secretId

getSecretField :: Client -> SecretIdentifier -> Text -> IO (Either Text ByteString)
getSecretField client secretId field = do
  logger' Trace loc ("GET " ++ show url') Nothing
  bt <- getBearer client
  resp <- catch (callFn bt) (handleError logger' loc onOtherThrow onOtherThrow)
  contentType <- maybe noContentType pure $ responseHeader resp "Content-Type"
  let contentType' = takeWhile (/= 59) contentType
  let body = responseBody resp :: ByteString
  case contentType' of
    "application/json" -> either (throwIO . BadJson200 . JsonHttpException) (pure . Left) $ eitherDecodeStrict body
    "application/octet-stream" -> pure $ Right body
    _ -> throwIO $ BadContentType (Just contentType') ["application/json", "application/octet-stream"]
  where
    loc = "Crypto.SecretServer.Sdk.Secret.getSecretField"
    logger' = fromJust $ logger client
    urlPort = url client
    url' = reqUrl urlPort /: "api" /: "v1" /: "secrets" /~ secretIdNum secretId /: "fields" /: field
    callFn bt =
      runReq defaultHttpConfig $
        req GET url' NoReqBody bsResponse (reqPort urlPort <> secretIdParam secretId <> tokenHeaderValue bt)
    noContentType = throwIO $ BadContentType Nothing ["application/json", "application/octet-stream"]

-- | Delete a secret
--
-- Secrets are never really deleted in Secret Server, only deactivated.
-- If you have the right permissions, you can view deactivated secrets and
-- permanently delete them in the UI.
deleteSecret :: Client -> SecretIdentifier -> IO DeletedModel
deleteSecret client secretId = do
  logger' Trace loc ("DELETE " ++ show url') Nothing
  bt <- getBearer client
  responseBody <$> catch (callFn bt) (handleError logger' loc onOtherThrow onOtherThrow)
  where
    logger' = fromJust $ logger client
    urlPort = url client
    url' = reqUrl urlPort /: "api" /: "v1" /: "secrets" /~ secretIdNum secretId
    callFn bt =
      runReq defaultHttpConfig $
        req DELETE url' NoReqBody jsonResponse (reqPort urlPort <> secretIdParam secretId <> tokenHeaderValue bt)
    loc = "Crypto.SecretServer.Sdk.Secret.getSecret"

moveSecret :: Client -> SecretIdentifier -> Int -> IO SecretDetailGeneralViewModel
moveSecret client secretId destFolder = updateSecret client secretId $ secretUpdateNothing {secretUpdateMany_folder = Just $ dirty destFolder}

updateSecret :: Client -> SecretIdentifier -> SecretDetailGeneralUpdateModel -> IO SecretDetailGeneralViewModel
updateSecret client secretId body = do
  logger' Trace loc ("PATCH " ++ show url') Nothing
  bt <- getBearer client
  responseBody <$> catch (callFn bt) (handleError logger' loc onOtherThrow onOtherThrow)
  where
    logger' = fromJust $ logger client
    urlPort = url client
    url' = reqUrl urlPort /: "api" /: "v1" /: "secrets" /~ secretIdNum secretId /: "general"
    callFn bt =
      runReq defaultHttpConfig $
        req PATCH url' (ReqBodyJson $ Data body) jsonResponse (reqPort urlPort <> secretIdParam secretId <> tokenHeaderValue bt)
    loc = "Crypto.SecretServer.Sdk.Secret.updateSecret"

updateSecretField ::
  Client ->
  SecretIdentifier ->
  -- | Field name
  Text ->
  -- | Field contents
  Either Text (FilePath, ByteString) ->
  IO (Maybe Text)
updateSecretField client secretId field value = do
  logger' Trace loc ("PUT " ++ show url') Nothing
  bt <- getBearer client
  resp <- catch (callFn bt value) (handleError logger' loc onOtherThrow onOtherThrow)
  contentType <- maybe noContentType pure $ responseHeader resp "Content-Type"
  when (takeWhile (/= 59) contentType /= "application/json") noContentType
  let body = responseBody resp :: ByteString
  case body of
    "true" -> pure Nothing
    _ -> either (throwIO . BadJson200 . JsonHttpException) (pure . Just) $ eitherDecodeStrict body
  where
    logger' = fromJust $ logger client
    urlPort = url client
    url' = reqUrl urlPort /: "api" /: "v1" /: "secrets" /~ secretIdNum secretId /: "fields" /: field
    loc = "Crypto.SecretServer.Sdk.Secret.updateSecretField"
    callFn bt (Right (name, contents)) = do
      reqBody <-
        reqBodyMultipart [partFileRequestBodyM "file" (takeFileName name) (pure $ RequestBodyBS contents)]
      runReq defaultHttpConfig $
        req PUT url' reqBody bsResponse (reqPort urlPort <> secretIdParam secretId <> tokenHeaderValue bt)
    callFn bt (Left fieldValue) =
      runReq defaultHttpConfig $
        req PUT url' (ReqBodyJson $ object ["value" .= fieldValue]) bsResponse (reqPort urlPort <> secretIdParam secretId <> tokenHeaderValue bt)
    noContentType = throwIO $ BadContentType Nothing ["application/json"]
