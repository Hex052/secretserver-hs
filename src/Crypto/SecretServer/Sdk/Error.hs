{-# LANGUAGE DeriveGeneric #-}

module Crypto.SecretServer.Sdk.Error (ApiError (..), Error4xxJson (..), handleError, onOtherThrow, onOtherReturnLeft, on404ReturnNothing) where

import Control.Exception (Exception (displayException), throw)
import Crypto.SecretServer.Sdk.Internal (optionsOmitNothing)
import Crypto.SecretServer.Sdk.Logging (LogFn, LogLevel (Error))
import Data.Aeson (FromJSON (parseJSON), Object, ToJSON (toEncoding, toJSON), decodeStrict, genericParseJSON, genericToEncoding, genericToJSON)
import Data.ByteString (ByteString)
import GHC.Generics (Generic)
import Network.HTTP.Client (HttpException (HttpExceptionRequest), HttpExceptionContent (StatusCodeException), responseStatus)
import Network.HTTP.Req (HttpException (JsonHttpException, VanillaHttpException))
import qualified Network.HTTP.Req as Req
import Network.HTTP.Types (statusCode)

-- | The API documentation has three different status codes that might be returned.
--
-- A 404 error should never be returned except by incorrect configuration.
-- If you requested a particular secret and it is not present, for example,
-- this will not
data ApiError
  = -- | We got a 200 response, but the JSON wasn't right or we tried to decode it to a bad object
    BadJson200 Req.HttpException
  | -- | Most likely, the data this library received was incorrect.
    Error400 Req.HttpException (Maybe Error4xxJson)
  | -- | We probably will have a brief message describing why it's forbidden
    Error403 Req.HttpException (Maybe Error4xxJson)
  | -- | This indicates an error on the part of the requestor.
    -- This might, depending on the design, be returned instead as a `Nothing`
    Error404 Req.HttpException
  | -- | When a 500 error occurs, we might get back some JSON information about it
    Error500 Req.HttpException (Maybe Object)
  | -- | Either there was some other error or we
    OtherError Req.HttpException
  | -- | Possible actual and list of expected content types
    BadContentType (Maybe ByteString) [ByteString]
  deriving (Show)

instance Exception ApiError

data Error4xxJson = Error4xxJson
  { -- | Error message
    message :: String,
    -- | Details about the error
    messageDetail :: Maybe String,
    -- | The unlocalized error code
    errorCode :: Maybe String,
    -- | Specific validation errors
    modelState :: Maybe Object
  }
  deriving (Generic, Show)

instance ToJSON Error4xxJson where
  toEncoding = genericToEncoding optionsOmitNothing
  toJSON = genericToJSON optionsOmitNothing

instance FromJSON Error4xxJson where
  parseJSON = genericParseJSON optionsOmitNothing

handleError ::
  LogFn ->
  String ->
  (Req.HttpException -> ApiError -> IO b) ->
  (Req.HttpException -> ApiError -> IO b) ->
  Req.HttpException ->
  IO b
handleError logFn source on404 onOther exc = do
  inner exc
  where
    -- inner :: HttpException -> IO ()
    inner (JsonHttpException msg) = do
      logFn Error source "http error: received bad json" (Just $ lines msg)
      onOther exc (BadJson200 exc)
    inner (VanillaHttpException innerexc) = do
      case innerexc of
        HttpExceptionRequest _ (StatusCodeException resp bs) -> badStatusCode resp bs
        _ -> somethingElse
      where
        badStatusCode resp bs = do
          logFn Error source "http error: bad status code" (Just $ show status : lines (displayException exc))
          case statusCode status of
            400 -> onOther exc $ Error400 exc $ decodeStrict bs
            403 -> onOther exc $ Error403 exc $ decodeStrict bs
            404 -> on404 exc $ Error404 exc
            500 -> onOther exc $ Error500 exc $ decodeStrict bs
            _ -> onOther exc $ OtherError exc
          where
            status = responseStatus resp
        somethingElse = do
          logFn Error source "http error" (Just $ lines $ displayException exc)
          onOther exc $ OtherError exc

onOtherReturnLeft :: Req.HttpException -> ApiError -> IO (Either ApiError b)
onOtherReturnLeft _ e = return $ Left e

onOtherThrow :: Req.HttpException -> ApiError -> IO b
onOtherThrow e _ = throw e

on404ReturnNothing :: Req.HttpException -> ApiError -> IO (Maybe b)
on404ReturnNothing _ _ = return Nothing
