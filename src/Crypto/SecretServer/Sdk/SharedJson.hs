{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Crypto.SecretServer.Sdk.SharedJson
  ( DeletedModel (..),
    Data (..),
    Dirty (..),
    dirty,
    -- WrappedValue (wrappedValue),
    ViewFieldValue (..),
    ViewFieldLink (..),
    UTCTimeWithoutZone (UTCTimeWithoutZone, wrappedTime),
    WrappedName (WrappedName, wrappedName),
  )
where

import Control.Applicative (Alternative ((<|>)))
import Crypto.SecretServer.Sdk.Internal (optionsOmitNothing, trimFront)
import Data.Aeson (FromJSON (parseJSON), KeyValue ((.=)), ToJSON (toJSON), genericParseJSON, genericToEncoding, genericToJSON, object, pairs, withObject, withText, (.:))
import Data.Aeson.Types (ToJSON (toEncoding))
import Data.Text (Text)
import Data.Time (UTCTime)
import Data.Time.FromText (parseUTCTime)
import GHC.Generics (Generic)
import Prelude hiding (String)

data DeletedModel = DeletedModel
  { -- | ID of the deleted object
    deletedSecret_id :: Int,
    -- | Type of the deleted object
    deletedSecret_objectType :: Text,
    -- | List of response codes from the delete operation
    deletedSecret_responseCodes :: [Text]
  }
  deriving (Generic, Read, Show, Eq, Ord)

instance ToJSON DeletedModel where
  toEncoding = genericToEncoding $ trimFront "deletedSecret_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "deletedSecret_" optionsOmitNothing

instance FromJSON DeletedModel where
  parseJSON = genericParseJSON $ trimFront "deletedSecret_" optionsOmitNothing

-- | A good amount of calls to Secret Server have the actual JSON body
-- wrapped in {"data":{}} for whatever reason. All this type does is add that.
newtype Data a = Data {wrappedData :: a}
  deriving (Read, Show, Eq, Ord)

instance (ToJSON a) => ToJSON (Data a) where
  toEncoding (Data a) = pairs ("data" .= a)
  toJSON (Data a) = object ["data" .= a]

instance (FromJSON a) => FromJSON (Data a) where
  parseJSON = withObject "Data" $ \v -> Data <$> v .: "data"

-- | Some responses provide the localized names of items as well.
--
-- Really, the most important ones are the `label`, `description`, and `value`

-- Generic for ViewFieldValueOf* in openapi file
data ViewFieldValue a = ViewFieldValue
  { -- | Seems to be usually null
    fieldValue_additionalLinks :: Maybe [ViewFieldLink],
    -- | Field description
    fieldValue_description :: Text,
    -- | Unsure what this is for, seems to be typically null.
    -- It's just described as FieldInputType in the docs.
    fieldValue_fieldInputType :: Maybe Text,
    -- | Only True or False on items that could have history
    fieldValue_hasHistory :: Maybe Bool,
    -- | Seems to be typically null and unsure how to set.
    fieldValue_helpLink :: Maybe Text,
    -- | Seems to be typically null and unsure how to set.
    fieldValue_helpLinkText :: Maybe Text,
    -- | I'm unsure if this can actually be Nothing, but it was marked as nullable
    fieldValue_hidden :: Maybe Bool,
    -- | Unclear what this does
    fieldValue_hideOnView :: Maybe Bool,
    -- | For fields this would be obvious, but this type is also returned
    -- for things like folder names (where it's null)
    fieldValue_isRequired :: Maybe Bool,
    -- | The field name
    fieldValue_label :: Text,
    -- | The maximum length of `value`, in characters
    fieldValue_maxLength :: Maybe Word,
    -- | Sometimes the value has a name for that particular value
    --
    -- For example "< No Policy >" in secretPolicy of SecretDetailGeneralViewModel
    fieldValue_name :: Maybe Text,
    -- | Seems to be typically null
    fieldValue_placeholder :: Maybe Text,
    -- | For ones that could be set, then this has a value of Just True or Just False
    -- (such as heartbeatEnabled of SecretDetailGeneralViewModel).
    -- But for ones that couldn't be set then this has a value of Nothing
    -- (such as active of SecretDetailGeneralViewModel)
    fieldValue_readonly :: Maybe Bool,
    -- | Haven't seen this not null. Possibly could be on maybe locked fields?
    fieldValue_readOnlyReason :: Maybe Text,
    fieldValue_sortOrder :: Maybe Int,
    -- | Field value
    fieldValue_value :: a
  }
  deriving (Generic, Read, Show, Eq, Ord)

instance (ToJSON a) => ToJSON (ViewFieldValue a) where
  toEncoding = genericToEncoding $ trimFront "fieldValue_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "fieldValue_" optionsOmitNothing

instance (FromJSON a) => FromJSON (ViewFieldValue a) where
  parseJSON = genericParseJSON $ trimFront "fieldValue_" optionsOmitNothing

instance Functor ViewFieldValue where
  fmap f v = v {fieldValue_value = f $ fieldValue_value v}

newtype WrappedValue a = WrappedValue {wrappedValue :: a}
  deriving (Read, Show, Eq, Ord)

instance (ToJSON a) => ToJSON (WrappedValue a) where
  toEncoding (WrappedValue a) = pairs ("value" .= a)
  toJSON (WrappedValue a) = object ["value" .= a]

instance (FromJSON a) => FromJSON (WrappedValue a) where
  parseJSON = withObject "WrappedValue" $ \v -> WrappedValue <$> v .: "value"

-- An list of this fills the additonalLinks field
data ViewFieldLink = ViewFieldLink
  { fieldLink_isExternal :: Bool,
    fieldLink_linkText :: Text,
    fieldLink_url :: Text
  }
  deriving (Generic, Read, Show, Eq, Ord)

instance ToJSON ViewFieldLink where
  toEncoding = genericToEncoding $ trimFront "fieldLink_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "fieldLink_" optionsOmitNothing

instance FromJSON ViewFieldLink where
  parseJSON = genericParseJSON $ trimFront "fieldLink_" optionsOmitNothing

-- | When updating a field, we must specify if the field needs updating
-- by setting `dirty` to `True`
data Dirty a = Dirty
  { -- | Whether or not the field is dirty.
    --
    -- If false, the field value will not be updated.
    -- If true, the field value will be updated.
    isDirty :: Bool,
    -- | The field value
    dirtyValue :: a
  }
  deriving (Read, Show, Eq, Ord)

-- | Creates a new `Dirty` with `isDirty` set to True, its most useful value
dirty :: a -> Dirty a
dirty = Dirty True

instance (ToJSON a) => ToJSON (Dirty a) where
  toEncoding (Dirty dirty' a) = pairs ("dirty" .= dirty' <> "value" .= a)
  toJSON (Dirty dirty' a) = object ["dirty" .= dirty', "value" .= a]

instance (FromJSON a) => FromJSON (Dirty a) where
  parseJSON = withObject "Dirty" $ \v -> Dirty <$> v .: "dirty" <*> v .: "value"

newtype UTCTimeWithoutZone = UTCTimeWithoutZone {wrappedTime :: UTCTime}
  deriving (Read, Show, Eq, Ord)

instance ToJSON UTCTimeWithoutZone where
  toEncoding (UTCTimeWithoutZone a) = toEncoding a
  toJSON (UTCTimeWithoutZone a) = toJSON a

instance FromJSON UTCTimeWithoutZone where
  parseJSON v = withText "UTCTimeWithoutZone" func v <|> withText "UTCTimeWithoutZone" (func . (<> "Z")) v
    where
      func = either fail (pure . UTCTimeWithoutZone) . parseUTCTime

newtype WrappedName a = WrappedName {wrappedName :: a}
  deriving (Read, Show, Eq, Ord)

instance (ToJSON a) => ToJSON (WrappedName a) where
  toEncoding (WrappedName a) = pairs ("name" .= a)
  toJSON (WrappedName a) = object ["name" .= a]

instance (FromJSON a) => FromJSON (WrappedName a) where
  parseJSON = withObject "WrappedName" $ \v -> WrappedName <$> v .: "name"
