{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

-- | This module contains all the models that the server returs and we have to send.
module Crypto.SecretServer.Sdk.Secret.Json
  ( DirtyField (..),
    dirtyField,
    SecretDetailGeneralUpdateModel (..),
    secretUpdateNothing,
    SecretModelV2 (..),
    RestSecretItem (..),
    SecretFieldListType (..),
    HeartbeatStatus (..),
    SecretDetailGeneralViewModel (..),
    TemplateField (..),
    SecretFieldType (..),
    Launcher (..),
  )
where

import Control.Applicative (Alternative ((<|>)))
import Crypto.SecretServer.Sdk.Internal (optionsOmitNothing, trimFront)
import Crypto.SecretServer.Sdk.SharedJson (Dirty, UTCTimeWithoutZone, ViewFieldValue)
import Data.Aeson (FromJSON (parseJSON), ToJSON (toEncoding, toJSON), Value (String), genericParseJSON, genericToEncoding, genericToJSON, object, pairs, withObject, withScientific, withText, (.:), (.=))
import Data.Scientific (toBoundedInteger)
import Data.Text (Text, pack, unpack)
import Data.UUID (UUID)
import GHC.Generics (Generic)
import Text.Read (readEither)

-- JSON sent

-- | Allows updating of some fields. Only compatable with text fields (no files).
--
-- Should probably be constructed via `dirtyField` as it saves one parameter
--
-- UpdateTemplateFieldOfString in the openapi file
data DirtyField = DirtyField
  { isDirty :: Bool,
    dirtySlug :: Text,
    dirtyValue :: Text
  }
  deriving (Read, Show, Eq, Ord)

-- | The `DirtyField` constructor but with `isDirty` set to `True`
dirtyField :: Text -> Text -> DirtyField
dirtyField = DirtyField True

instance ToJSON DirtyField where
  toEncoding (DirtyField dirty' slug' value') = pairs ("dirty" .= dirty' <> "slug" .= slug' <> "value" .= value')
  toJSON (DirtyField dirty' slug' value') = object ["dirty" .= dirty', "slug" .= slug', "value" .= value']

instance FromJSON DirtyField where
  parseJSON = withObject "Dirty" $ \v -> DirtyField <$> v .: "dirty" <*> v .: "slug" <*> v .: "value"

data SecretDetailGeneralUpdateModel = SecretDetailGeneralUpdateModel
  { secretUpdateMany_active :: Maybe (Dirty Bool),
    secretUpdateMany_enableInheritSecretPolicy :: Maybe (Dirty Bool),
    secretUpdateMany_folder :: Maybe (Dirty Int),
    -- | When true autogenerate new SSH keys
    secretUpdateMany_generateSshKeys :: Maybe Bool,
    secretUpdateMany_heartbeatEnabled :: Maybe (Dirty Bool),
    secretUpdateMany_isOutOfSync :: Maybe (Dirty Bool),
    secretUpdateMany_name :: Maybe (Dirty Text),
    -- | Secret Fields
    secretUpdateMany_secretFields :: Maybe [DirtyField],
    secretUpdateMany_secretPolicy :: Maybe (Dirty Int),
    secretUpdateMany_site :: Maybe (Dirty Int),
    secretUpdateMany_template :: Maybe (Dirty Int)
  }
  deriving (Generic, Read, Show, Eq)

-- | The default empty one, as a starting point to actually updating things
secretUpdateNothing :: SecretDetailGeneralUpdateModel
secretUpdateNothing = SecretDetailGeneralUpdateModel Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing

instance ToJSON SecretDetailGeneralUpdateModel where
  toEncoding = genericToEncoding $ trimFront "secretUpdateMany_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "secretUpdateMany_" optionsOmitNothing

instance FromJSON SecretDetailGeneralUpdateModel where
  parseJSON = genericParseJSON $ trimFront "secretUpdateMany_" optionsOmitNothing

-- JSON received

data SecretModelV2 = SecretModelV2
  { -- | Access Request Workflow Map Id
    secretModel_accessRequestWorkflowMapId :: Maybe Int,
    -- | Whether the secret is active
    secretModel_active :: Bool,
    -- | Allow Owners Unrestricted SSH Commands
    secretModel_allowOwnersUnrestrictedSshCommands :: Bool,
    -- | Auto Change Enabled
    secretModel_autoChangeEnabled :: Bool,
    -- | Auto Change Next Password
    secretModel_autoChangeNextPassword :: Bool,
    -- | Whether the secret is currently checked out
    secretModel_checkedOut :: Bool,
    -- | Check Out Change Password Enabled
    secretModel_checkOutChangePasswordEnabled :: Bool,
    -- | Whether secret checkout is enabled
    secretModel_checkOutEnabled :: Bool,
    -- | Checkout interval, in minutes
    secretModel_checkOutIntervalMinutes :: Int,
    -- | Minutes remaining in current checkout interval
    secretModel_checkOutMinutesRemaining :: Int,
    -- | Name of user who has checked out the secret
    secretModel_checkOutUserDisplayName :: Text,
    -- | ID of user who has checked out the secret
    secretModel_checkOutUserId :: Int,
    -- | DoubleLock Id
    secretModel_doubleLockId :: Int,
    -- | Enable Inherit Permissions
    secretModel_enableInheritPermissions :: Bool,
    -- | Whether the secret policy is inherited from the containing folder
    secretModel_enableInheritSecretPolicy :: Bool,
    -- | Number of failed password change attempts
    secretModel_failedPasswordChangeAttempts :: Int,
    -- | Containing folder ID
    secretModel_folderId :: Int,
    -- | Secret ID
    secretModel_id :: Int,
    -- | Whether double lock is enabled
    secretModel_isDoubleLock :: Bool,
    -- | Out of sync indicates that a Password is setup for autochange and has failed its last password change attempt or has exceeded the maximum RPC attempts
    secretModel_isOutOfSync :: Bool,
    -- | Whether the secret is restricted
    secretModel_isRestricted :: Bool,
    -- | Secret data fields
    secretModel_items :: [RestSecretItem],
    -- | Jumpbox Route Id
    secretModel_jumpboxRouteId :: Maybe UUID,
    -- | Time of last heartbeat check
    secretModel_lastHeartBeatCheck :: Maybe UTCTimeWithoutZone,
    secretModel_lastHeartBeatStatus :: HeartbeatStatus,
    -- | Time of most recent password change attempt
    secretModel_lastPasswordChangeAttempt :: Maybe UTCTimeWithoutZone,
    secretModel_launcherConnectAsSecretId :: Maybe Int,
    -- | Secret name
    secretModel_name :: Text,
    -- | Reason message if the secret is out of sync
    secretModel_outOfSyncReason :: Text,
    -- | Password Type Web Script Id
    secretModel_passwordTypeWebScriptId :: Int,
    -- | Proxy Enabled
    secretModel_proxyEnabled :: Bool,
    -- | Requires Approval For Access
    secretModel_requiresApprovalForAccess :: Bool,
    -- | Requires Comment
    secretModel_requiresComment :: Bool,
    -- | Response Codes
    secretModel_responseCodes :: [Text],
    -- | Restrict SSH Commands
    secretModel_restrictSshCommands :: Bool,
    -- | Secret Policy Id
    secretModel_secretPolicyId :: Int,
    -- | Secret template ID
    secretModel_secretTemplateId :: Int,
    -- | Name of secret template
    secretModel_secretTemplateName :: Text,
    -- | Whether session recording is enabled
    secretModel_sessionRecordingEnabled :: Bool,
    -- | Site Id
    secretModel_siteId :: Int,
    -- | Web Launcher Requires Incognito Mode
    secretModel_webLauncherRequiresIncognitoMode :: Bool
  }
  deriving (Generic, Read, Show, Eq)

instance ToJSON SecretModelV2 where
  toEncoding = genericToEncoding $ trimFront "secretModel_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "secretModel_" optionsOmitNothing

instance FromJSON SecretModelV2 where
  parseJSON = genericParseJSON $ trimFront "secretModel_" optionsOmitNothing

data RestSecretItem = RestSecretItem
  { -- | Longer description of the secret field.
    secretField_fieldDescription :: Text,
    -- | The id of the field definition from the secret template.
    secretField_fieldId :: Maybe Int,
    -- | The display name of the secret field.
    secretField_fieldName :: Text,
    -- | If the field is a file attachment field, the id of the file attachment.
    secretField_fileAttachmentId :: Maybe Int,
    -- | If the field is a file attachment field, the name of the attached file.
    secretField_filename :: Text,
    -- | Whether the field is a file attachment.
    secretField_isFile :: Bool,
    -- | Whether or not the secret field is a list.
    secretField_isList :: Bool,
    -- | Whether the field is represented as a multi-line text box. Used for long-form text fields.
    secretField_isNotes :: Bool,
    -- | Whether the field is a password.
    --
    -- Password fields are hidden by default in the UI and their value is not returned in calls that return secrets.
    -- To retrieve a password field value, make a call to `getSecretField`
    secretField_isPassword :: Bool,
    -- | The id of the secret field item. Leave empty when creating a new secret.
    secretField_itemId :: Maybe Int,
    -- | The value of the secret field item. For list fields, this is a comma-delimited list of the list id guids that are assigned to this field.
    secretField_itemValue :: Text,
    secretField_listType :: SecretFieldListType,
    -- | A unique name for the secret field on the template. Slugs cannot contain spaces and are used in many places to easily refer to a secret field without havinto know the field id.",
    secretField_slug :: Text
  }
  deriving (Generic, Read, Show, Eq)

instance ToJSON RestSecretItem where
  toEncoding = genericToEncoding $ trimFront "secretField_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "secretField_" optionsOmitNothing

instance FromJSON RestSecretItem where
  parseJSON = genericParseJSON $ trimFront "secretField_" optionsOmitNothing

data HeartbeatStatus
  = Failed
  | Success
  | Pending
  | Disabled
  | UnableToConnect
  | UnknownError
  | IncompatibleHost
  | AccountLockedOut
  | DnsMismatch
  | UnableToValidateServerPublicKey
  | Processing
  | ArgumentError
  | AccessDenied
  deriving (Generic, Read, Show, Eq, Ord, Enum)

instance ToJSON HeartbeatStatus where
  toEncoding = genericToEncoding optionsOmitNothing
  toJSON = genericToJSON optionsOmitNothing

instance FromJSON HeartbeatStatus where
  parseJSON = genericParseJSON optionsOmitNothing

-- |
--
-- Name in the openapi file is SecretDetailGeneralViewModel
data SecretDetailGeneralViewModel = SecretDetailGeneralViewModel
  { secretGeneralModel_active :: ViewFieldValue Bool,
    secretGeneralModel_autoChangePassword :: ViewFieldValue Bool,
    secretGeneralModel_canGenerateSshKey :: Bool,
    secretGeneralModel_enableInheritSecretPolicy :: ViewFieldValue Bool,
    -- secretGeneralModel_expiration :: ViewFieldValue Text, -- This one decides to break the rules and have every single field null.
    secretGeneralModel_fields :: [TemplateField],
    secretGeneralModel_folder :: ViewFieldValue Int,
    secretGeneralModel_heartbeatEnabled :: ViewFieldValue Bool,
    secretGeneralModel_id :: Int,
    secretGeneralModel_isFavorite :: Bool,
    secretGeneralModel_isOutOfSync :: ViewFieldValue Bool,
    secretGeneralModel_isTotpEnabled :: Maybe Bool,
    secretGeneralModel_lastHeartBeatCheck :: ViewFieldValue (Maybe UTCTimeWithoutZone),
    secretGeneralModel_lastHeartBeatStatus :: ViewFieldValue HeartbeatStatus,
    secretGeneralModel_launchers :: [Launcher],
    secretGeneralModel_name :: ViewFieldValue Text,
    secretGeneralModel_outOfSyncReason :: Maybe (ViewFieldValue Text),
    secretGeneralModel_secretPolicy :: ViewFieldValue (Maybe Int),
    secretGeneralModel_site :: ViewFieldValue Int,
    -- | The name of the field that has a private key in it
    secretGeneralModel_slugPrivateKey :: Maybe Text,
    -- | The name of the field that has a public key in it
    secretGeneralModel_slugPublicKey :: Maybe Text,
    secretGeneralModel_template :: ViewFieldValue Int,
    -- | The name of the field that has a TOTP key
    secretGeneralModel_totpPasswordSlug :: Maybe Text
  }
  deriving (Generic, Read, Show, Eq)

instance ToJSON SecretDetailGeneralViewModel where
  toEncoding = genericToEncoding $ trimFront "secretGeneralModel_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "secretGeneralModel_" optionsOmitNothing

instance FromJSON SecretDetailGeneralViewModel where
  parseJSON = genericParseJSON $ trimFront "secretGeneralModel_" optionsOmitNothing

data TemplateField = TemplateField
  { -- templateField_additionalLinks :: Maybe ViewFieldLink, -- Seems to be always null
    templateField_description :: Text,
    -- templateField_dropdownOptions :: Maybe DropdownOptions, -- named dropDownOptions
    -- templateField_fieldInputType :: Maybe Text,
    templateField_hasHistory :: Bool,
    -- templateField_helpLink :: Maybe Text, -- These seem to have no way to set them and are always null
    -- templateField_helpLinkText :: Maybe Text,
    templateField_hidden :: Bool,
    -- -- | I have no idea what this controls. Can't figure out how to set it from False either
    -- templateField_hideOnView :: Bool,
    templateField_isRequired :: Bool,
    templateField_label :: Text,
    templateField_listType :: SecretFieldListType,
    -- templateField_maxLength :: Maybe Word32 -- Seems to be always null without a way to set
    templateField_name :: Text,
    -- | For password fields (where `type` is `Password`), indicates what
    -- requirements there are when this value is set.
    templateField_passwordRequirementId :: Maybe Int,
    templateField_placeholder :: Maybe Text, -- Seems to be always null
    templateField_readOnly :: Bool,
    templateField_readOnlyReason :: Maybe Text, -- Seems to be always null
    templateField_slug :: Text,
    templateField_sortOrder :: Maybe Int,
    templateField_type :: SecretFieldType,
    -- | If the `type'` is `List`, `Password` or `File`, this is not present.
    templateField_value :: Maybe Text
  }
  deriving (Generic, Read, Show, Eq)

instance ToJSON TemplateField where
  toEncoding = genericToEncoding $ trimFront "templateField_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "templateField_" optionsOmitNothing

instance FromJSON TemplateField where
  parseJSON = genericParseJSON $ trimFront "templateField_" optionsOmitNothing

data SecretFieldType = Text | Notes | Url | Password | File | DropDown | List
  deriving (Read, Show, Eq, Ord, Enum, Generic)

instance ToJSON SecretFieldType where
  toEncoding = genericToEncoding optionsOmitNothing
  toJSON = genericToJSON optionsOmitNothing

instance FromJSON SecretFieldType where
  parseJSON = genericParseJSON optionsOmitNothing

data SecretFieldListType = None | Generic | URL
  deriving (Read, Show, Eq, Ord, Enum)

instance ToJSON SecretFieldListType where
  toJSON = String . pack . show

instance FromJSON SecretFieldListType where
  parseJSON v =
    withText "SecretFieldListType" (either fail pure . readEither . unpack) v
      <|> withScientific "SecretFieldListType" (maybe (fail "not integer number") (pure . toEnum) . toBoundedInteger) v

data Launcher = Launcher
  { launcher_hasProxyCredentials :: Bool,
    -- | URL to an icon
    launcher_imagePath :: Text,
    launcher_isRecorded :: Bool,
    launcher_name :: Text,
    launcher_showLauncher :: Bool,
    -- | The ID used for /v1/launchers/types/{launcherTypeId}
    launcher_typeId :: Int
  }
  deriving (Generic, Read, Show, Eq)

instance ToJSON Launcher where
  toEncoding = genericToEncoding $ trimFront "launcher_" optionsOmitNothing
  toJSON = genericToJSON $ trimFront "launcher_" optionsOmitNothing

instance FromJSON Launcher where
  parseJSON = genericParseJSON $ trimFront "launcher_" optionsOmitNothing
