{-# LANGUAGE OverloadedStrings #-}

-- | Sole purpose of this module is to retrieve a bearer token to be used with other calls
module Crypto.SecretServer.Sdk.OAuth
  ( getBearerTokenFromSdkClientAccount,
    getBearerTokenFromUsernamePassword,
    getBearerTokenFromRefreshToken,
  )
where

import Control.Exception (catch)
import Crypto.SecretServer.Sdk.Error (handleError, onOtherThrow)
import Crypto.SecretServer.Sdk.Logging (LogFn, LogLevel (Debug, Trace))
import Crypto.SecretServer.Sdk.OAuth.Json (BearerTokenExpiration, toBearerTokenExpiration)
import Crypto.SecretServer.Sdk.SdkAccounts.Json (SdkClientAccountModel (clientAccount_clientId, clientAccount_clientSecret))
import Crypto.SecretServer.Sdk.Types (UrlPort, reqPort, reqUrl)
import Data.Text (Text, unpack)
import Network.HTTP.Req (FormUrlEncodedParam, POST (POST), ReqBodyUrlEnc (ReqBodyUrlEnc), defaultHttpConfig, jsonResponse, req, responseBody, runReq, (/:), (=:))

fromSdkClientAccount :: SdkClientAccountModel -> FormUrlEncodedParam
fromSdkClientAccount creds =
  "client_id" =: ("sdk-client-" ++ show (clientAccount_clientId creds))
    <> "client_secret" =: clientAccount_clientSecret creds
    <> "grant_type" =: ("client_credentials" :: Text)

fromUsernamePassword :: Text -> Text -> FormUrlEncodedParam
fromUsernamePassword usr pwd =
  "username" =: usr
    <> "password" =: pwd
    <> "grant_type" =: ("password" :: Text)

fromRefreshToken :: Text -> FormUrlEncodedParam
fromRefreshToken token =
  "refresh_token" =: token
    <> "grant_type" =: ("refresh_token" :: Text)

-- -- | For the record,  I have no idea what this is. I don't even know how one
-- -- might create an authorization code. This isn't docuemnted in the official API
-- -- documentation (though nor is how `fromSdkClientAccount` works documented)
-- --
-- -- You can verify this is how it works in the .NET client by creating a
-- -- `Thycotic.SecretServer.Sdk.Infrastructure.Models.AuthorizationCodeCredentials`
-- -- and passing it to
-- -- `Thycotic.SecretServer.Sdk.Infrastructure.Clients.OAuthClient.GetAccessToken(AuthorizationCodeCredentials)`
-- -- then observing the resulting JSON through something like https://requestbin.com
-- fromAuthorizationCode :: Text -> Text -> Text -> Text -> FormUrlEncodedParam
-- fromAuthorizationCode code redirectUri clientId' clientSecret' =
--   "code" =: code
--     <> "redirect_uri" =: redirectUri
--     <> "client_id" =: clientId'
--     <> "client_secret" =: clientSecret'
--     <> "grant_type" =: ("authorization_code" :: Text)

getBearerTokenFromSdkClientAccount :: LogFn -> UrlPort -> SdkClientAccountModel -> IO BearerTokenExpiration
getBearerTokenFromSdkClientAccount logger url model = do
  logger Debug "Crypto.SecretServer.Sdk.OAuth.getBearerTokenFromSdkClientAccount" "Getting token" (Just ["clientId: " ++ show (clientAccount_clientId model)])
  getBearerToken logger url $ fromSdkClientAccount model

getBearerTokenFromUsernamePassword ::
  LogFn ->
  UrlPort ->
  -- | Username
  Text ->
  -- | Password
  Text ->
  IO BearerTokenExpiration
getBearerTokenFromUsernamePassword logger url usr passwd = do
  logger Debug "Crypto.SecretServer.Sdk.OAuth.getBearerTokenFromUsernamePassword" "Getting token" (Just ["user: " ++ unpack usr])
  getBearerToken logger url $ fromUsernamePassword usr passwd

getBearerTokenFromRefreshToken :: LogFn -> UrlPort -> Text -> IO BearerTokenExpiration
getBearerTokenFromRefreshToken logger url token = do
  logger Debug "Crypto.SecretServer.Sdk.OAuth.getBearerTokenFromRefreshToken" "Refreshing token" Nothing
  getBearerToken logger url $ fromRefreshToken token

getBearerToken :: LogFn -> UrlPort -> FormUrlEncodedParam -> IO BearerTokenExpiration
getBearerToken logger partialClient body = do
  logger Trace loc ("POST " ++ show url) Nothing
  tokenResponse <- catch callFn (handleError logger loc onOtherThrow onOtherThrow)
  toBearerTokenExpiration $ responseBody tokenResponse
  where
    url = reqUrl partialClient /: "oauth2" /: "token"
    callFn =
      runReq defaultHttpConfig $
        req POST url (ReqBodyUrlEnc body) jsonResponse (reqPort partialClient)
    loc = "Crypto.SecretServer.Sdk.OAuth.getBearerToken"
