{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Crypto.SecretServer.Sdk.SdkAccounts (create) where

import Control.Exception (catch)
import Crypto.SecretServer.Sdk.Error (handleError, onOtherThrow)
import Crypto.SecretServer.Sdk.Logging (LogFn, LogLevel (Info, Trace))
import Crypto.SecretServer.Sdk.SdkAccounts.Json (ClientCreateArgs (ClientCreateArgs, clientCreate_onboardingKey), SdkClientAccountModel)
import Crypto.SecretServer.Sdk.Types (UrlPort, reqPort, reqUrl)
import Data.Text (Text)
import Data.UUID (UUID)
import Data.UUID.V4 (nextRandom)
import Network.HTTP.Req (POST (POST), ReqBodyJson (ReqBodyJson), defaultHttpConfig, jsonResponse, req, responseBody, runReq, (/:))

create ::
  LogFn ->
  UrlPort ->
  Maybe UUID ->
  Text ->
  Text ->
  Maybe Text ->
  Text ->
  -- IO (Either ApiError (JsonResponse SdkClientAccountModel))
  IO SdkClientAccountModel
create logf partialClient Nothing description name onboardingKey' ruleName =
  nextRandom >>= (\x -> create logf partialClient (Just x) description name onboardingKey' ruleName)
create logf partialClient (Just clientId) description name onboardingKey' ruleName = do
  logf Info "Crypto.SecretServer.Sdk.SdkAccounts.create" ("Request to create with name " ++ show name ++ " for rule " ++ show ruleName) Nothing
  logf Trace "Crypto.SecretServer.Sdk.SdkAccounts.create" ("POST " ++ show url) (Just [show $ args {clientCreate_onboardingKey = clientCreate_onboardingKey args >>= const (Just "****")}])
  -- catch (callFn <&> Right) (handleError logf "Crypto.SecretServer.Sdk.SdkAccounts.create" (const (return . Left)) (const (return . Left)))
  responseBody <$> catch callFn (handleError logf "Crypto.SecretServer.Sdk.SdkAccounts.create" onOtherThrow onOtherThrow)
  where
    args = ClientCreateArgs clientId description name onboardingKey' ruleName
    body = ReqBodyJson args
    url = reqUrl partialClient /: "api" /: "v1" /: "sdk-client-accounts"
    callFn =
      runReq defaultHttpConfig $
        req POST url body jsonResponse (reqPort partialClient)
