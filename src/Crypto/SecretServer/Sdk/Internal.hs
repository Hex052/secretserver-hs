{-# LANGUAGE OverloadedStrings #-}

module Crypto.SecretServer.Sdk.Internal (optionsOmitNothing, trimFront, extendedModel) where

import Data.Aeson (Options (fieldLabelModifier, omitNothingFields), defaultOptions)
import Data.List (stripPrefix)
import Data.Maybe (fromMaybe)
import Network.HTTP.Req (Option, header)

-- | The default options, but omit any values that are Nothing, making the
-- | transmitted JSON smaller
optionsOmitNothing :: Options
optionsOmitNothing =
  defaultOptions
    { -- A good number of JSON fields are usually empty
      -- (such as `SecretDetailGeneralUpdateModel`) and can be omitted
      omitNothingFields = True,
      -- Strip all apostrophes from field names, as those are there to avoid
      -- name conflicts
      fieldLabelModifier = \x -> if last x == '\'' then init x else x
    }

-- | Trims a prefix off of all fields
trimFront :: String -> Options -> Options
trimFront prefix opt = opt {fieldLabelModifier = strip}
  where
    strip f = fieldLabelModifier opt $ fromMaybe f $ stripPrefix prefix f

extendedModel :: Option scheme
extendedModel = header "extended-model" "extended"
