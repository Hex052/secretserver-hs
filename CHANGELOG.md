# Revision history for secretserver

## 0.0.1.2 -- 2023-12-13

* Fix `sscli secret get`
* Fix URL parser for `sscli init -u`
* Add output and input options
* Print nicely the output when not as JSON
* Trim trailing newlines when reading from stdin to set secrets

## 0.0.1.0 -- 2023-12-01

* First version. Released on an unsuspecting world.
* The very basics that are required to read and write to secrets.
