## TODO list
- Custom CA certs. This should be added, though would require adding support to `req` or `http-client` to do that easily.
  - I refuse to add the ability to disable the cacert check entirely.
- Proxy support. This is supported by `req` though not added to this library (and therefore not `sscli init`)
- Installers. As msi, deb, rpm.
- Support for the old file format and migration from it
