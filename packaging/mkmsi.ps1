cabal build exe:sscli
if ($LASTEXITCODE -ne 0) {
	Write-Error "cabal build failed"
	exit 1
}
$sscli = cabal list-bin exe:sscli
$ssversion = (Select-String -Pattern '^version: *([^ ]*)$' secretserver.cabal).Matches[0].Groups[1].Value

@"
<?xml version="1.0" encoding="UTF-8"?>
<Wix xmlns="http://wixtoolset.org/schemas/v4/wxs">
  <Package
			ProductCode="e79ce6fa-23f0-4ea8-89bb-231c13b7a7ba"
			Language="1033"
			Manufacturer="Hex"
			Name="Secret Server CLI"
			Version="${ssversion}"
			Scope="perMachine"
			Compressed="yes"
			UpgradeCode="5716d644-ef00-40db-ba07-90dfa9a4bb0e">

		<MediaTemplate EmbedCab="yes" />

		<StandardDirectory Id="ProgramFiles64Folder">
			<Directory Id="INSTALLDIR" Name="sscli">
				<Component Id="ProductComponent">
					<File KeyPath="yes" Source="${sscli}"></File>
				</Component>
				<Component Id="PathComponent" Guid="7c04fa2d-90ac-4c56-8c3e-aa871aa0aafd">
					<Environment Id="PATH" Name="PATH" Value="[INSTALLDIR]" Permanent="no" Part="last" Action="set" System="yes" />
				</Component>
			</Directory>
		</StandardDirectory>

		<Feature Id="sscliFeature" Title="sscli" Display="expand">
			<ComponentRef Id="ProductComponent" />
			<Feature Id="PathFeature" Title="Add to PATH">
				<ComponentRef Id="PathComponent" />
			</Feature>
		</Feature>

	</Package>
</Wix>
"@ | Out-File sscli-${ssversion}_x64.wxs -Encoding utf8

wix build -arch x64 sscli-${ssversion}_x64.wxs
$e = $LASTEXITCODE
Remove-Item sscli-${ssversion}_x64.wxs
exit $e
