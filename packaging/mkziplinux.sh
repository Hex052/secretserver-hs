#!/bin/sh
if ! cabal build exe:sscli ; then
  echo "build failed" >&2
	return 1
fi

ssversion="`grep '^version:' secretserver.cabal | grep -o '[^ ]*$'`"
sscli="`cabal list-bin exe:sscli`"

tar -cJf "sscli-${ssversion}-1_amd64.tar.xz" -C "`dirname "${sscli}"`" "`basename "${sscli}"`"
