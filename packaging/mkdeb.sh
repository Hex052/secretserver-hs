#!/bin/sh
if ! cabal build exe:sscli ; then
  echo "build failed" >&2
	return 1
fi

ssversion="`grep '^version:' secretserver.cabal | grep -o '[^ ]*$'`"
dir="`mktemp -d`"
# pkgname="`grep '^name:' secretserver.cabal | grep -o '[^ ]*$'`"
sscli="`cabal list-bin exe:sscli`"

# Executable
# https://www.internalpointers.com/post/build-binary-deb-package-practical-guide
debdir="${dir}/sscli-${ssversion}-1_amd64"
mkdir "$debdir"
mkdir -p "$debdir/usr/bin"
cp "$sscli" "$debdir/usr/bin"
# mkdir -p "$debdir/usr/share/sscli" # only needed if wasn't statically linked.

# Completions
bashcompletionsdir="/usr/share/bash-completion/"
# bashcompletionsdir="/etc/bash_completion.d/"
mkdir -p "$debdir/$bashcompletionsdir"
"$sscli" --bash-completion-script /usr/bin/sscli > "$debdir/$bashcompletionsdir/sscli"

# Control files
mkdir "$debdir/DEBIAN"
cat << EOF > "$debdir/DEBIAN/control" # https://www.debian.org/doc/debian-policy/ch-controlfields.html
Package: sscli
Section: misc
Version: ${ssversion}
Homepage: https://gitlab.com/Hex052/secretserver-hs/
Architecture: amd64
Maintainer: Hex <elijahiff@gmail.com>
Description: Executable for access to Secret Server programatically,
 without requiring Kerberos (Windows) authentication or your
 script/application to need to deal with API keys and renewing them.
EOF
dpkg-deb --build --root-owner-group "$debdir"
mv "$debdir.deb" .
rm -rf "$dir"
