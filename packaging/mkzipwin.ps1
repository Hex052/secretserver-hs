cabal build exe:sscli
if ($LASTEXITCODE -ne 0) {
	Write-Error "cabal build failed"
	exit 1
}

$sscli = cabal list-bin exe:sscli
$ssversion = (Select-String -Pattern '^version: *([^ ]*)$' secretserver.cabal).Matches[0].Groups[1].Value

Compress-Archive $sscli sscli-${ssversion}_x64.zip
