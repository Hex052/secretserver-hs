# Notes on the API
Sometimes the API behaves differently than is documented. Here I record the differences I have found.
In particular, there is an undocumented header `extended-model: extended` that changes some
return values to be more detailed.

This is certainly not exhastive, just the ones I've come across so far.

## Secrets
- include the header `extended-model: extended` in request to make it return
`ViewFieldValue` rather than just the value:
  - GET `/v2/secret-templates/{secretTemplateId}`

## Templates

- include the header `extended-model: extended` in request to make it return
`ViewFieldValue` rather than just the value:
  - GET `/v2/secret-templates/{secretTemplateId}`
  - GET `/v1/secret-templates/fields/{secretFieldId}`
